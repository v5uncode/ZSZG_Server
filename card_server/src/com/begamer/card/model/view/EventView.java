package com.begamer.card.model.view;

public class EventView {
	/** **副本id***挑战次数******* */
	public int id;

	public String name;

	public int num;
	
	public float mul;//活动副本掉落倍率
	
	public static EventView creatEventMulView(int id,String name,float mul)
	{
		EventView eventView = new EventView();
		eventView.setId(id);
		eventView.setName(name);
		eventView.setMul(mul);
		return eventView;
	}

	public static EventView creatEventView(int id,String name,int num)
	{
		EventView eventView = new EventView();
		eventView.setId(id);
		eventView.setName(name);
		eventView.setNum(num);
		return eventView;
	}
	
	
	public float getMul()
	{
		return mul;
	}

	public void setMul(float mul)
	{
		this.mul = mul;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getNum()
	{
		return num;
	}

	public void setNum(int num)
	{
		this.num = num;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}
}
