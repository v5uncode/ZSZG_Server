package com.begamer.card.model.dbservice.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.begamer.card.model.dao.UserDao;
import com.begamer.card.model.dbservice.UserService;
import com.begamer.card.model.pojo.User;

public class UserServiceImpl extends AbstractService implements UserService {

	@Autowired
	private UserDao userDao;

	public User getUserByNameAndPassword(String name, String password) {
		String where = " name ='" + name + "' and password ='" + password + "'";
		String orderBy = null;
		List<User> list = userDao.load(0, 0, where, orderBy);
		if (list != null && list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public User read(Integer userid) {

		return userDao.read(userid);
	}

	public void update(User user) {

		userDao.update(user);
	}

	public User getUserByName(String userName) {
		String where = " name ='" + userName + "'";
		String orderBy = null;
		List<User> list = userDao.load(0, 0, where, orderBy);
		if (list != null && list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	@Override
	public void create(User user) {
		// TODO Auto-generated method stub
		userDao.create(user);
	}

	@Override
	public void delete(User user) {
		// TODO Auto-generated method stub
		userDao.delete(user);
	}

	@Override
	public List<User> list() {
		// TODO Auto-generated method stub
		return userDao.load(0, 0, null, null);
	}

}
