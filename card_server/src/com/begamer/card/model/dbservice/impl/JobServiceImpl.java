package com.begamer.card.model.dbservice.impl;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import com.begamer.card.model.dbservice.UserService;

public class JobServiceImpl implements Job {
	private Logger logger = Logger.getLogger(JobServiceImpl.class);
	@Autowired
	private UserService userService;

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub
	}

	public void test() {
		logger.debug("userService:" + userService);
		logger.debug("user.name:" + userService.read(1).getName());
	}
}
