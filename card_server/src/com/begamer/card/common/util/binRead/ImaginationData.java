package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.StringUtil;

public class ImaginationData implements PropertyReader
{
	/**冥想编号**/
	public int index;
	/**金币消耗**/
	public int spend;
	/**成功几率**/
	public int probability;
	/**物品类型-出现等级-物品id-掉落几率**/
	public List<String> item_info;
	
	private static HashMap<Integer, ImaginationData> data =new HashMap<Integer, ImaginationData>();
	@Override
	public void addData() {
		data.put(index, this);
	}
	@Override
	public void parse(String[] ss) {
		int location=0;
		index =StringUtil.getInt(ss[location]);
		spend =StringUtil.getInt(ss[location+1]);
		probability =StringUtil.getInt(ss[location+2]);
		item_info =new ArrayList<String>();
		for(int i=0;i<7;i++)
		{
			location =3+i*4;
			int type =StringUtil.getInt(ss[location]);
			if(type !=0)
			{
				int level =StringUtil.getInt(ss[location+1]);
				int item =StringUtil.getInt(ss[location+2]);
				int pro =StringUtil.getInt(ss[location+3]);
				String itemsInfo =type+"-"+level+"-"+item+"-"+pro; 
				item_info.add(itemsInfo);
			}
			else
			{
				continue;
			}
		}
		addData();
	}

	@Override
	public void resetData() {
		data.clear();
	}
	/**根据编号index获取一个imaginationData**/
	public static ImaginationData getImaginationData(int id)
	{
		return data.get(id);
	}

}
