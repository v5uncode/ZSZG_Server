package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class EnergyData implements PropertyReader
{

	public int type;
	public String typeDes;
	public float energySingle;
	public float energySwing;
	public float energyLine;
	public float energyAll;
	
	public float[] energys;
	
	/**单体**/
	public static final int ScopeSingle=0;
	/**竖排**/
	public static final int ScopeSwing=1;
	/**横排**/
	public static final int ScopeLine=2;
	/**全体**/
	public static final int ScopeAll=3;
	
	private static HashMap<Integer, EnergyData> data=new HashMap<Integer, EnergyData>();
	
	public void addData()
	{
		energys=new float[4];
		energys[0]=energySingle;
		energys[1]=energySwing;
		energys[2]=energyLine;
		energys[3]=energyAll;
		data.put(type,this);
	}
	public void resetData()
	{
		data.clear();
	}
	public void parse(String[] ss)
	{
	}
	
	public static float getEnergy(int type,int scope)
	{
		EnergyData ed=data.get(type);
		if(ed!=null)
		{
			return ed.energys[scope];
		}
		return 0;
	}

}
