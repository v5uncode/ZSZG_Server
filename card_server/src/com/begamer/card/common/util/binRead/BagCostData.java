package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class BagCostData implements PropertyReader
{
	/**购买次数**/
	public int number;
	/**扩充格数**/
	public int number1;
	/**花费类型:1钻石,2金币**/
	public int type;
	/**花费**/
	public int cost;

	private static HashMap<Integer, BagCostData> data=new HashMap<Integer, BagCostData>();
	
	@Override
	public void addData()
	{
		data.put(number, this);
	}

	@Override
	public void parse(String[] ss)
	{

	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static BagCostData getData(int number)
	{
		return data.get(number);
	}
	
}
