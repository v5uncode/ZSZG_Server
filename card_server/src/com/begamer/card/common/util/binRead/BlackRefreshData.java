package com.begamer.card.common.util.binRead;

import java.util.HashMap;

import com.sun.xml.internal.bind.v2.model.core.ID;

public class BlackRefreshData implements PropertyReader
{
	public int number;
	public int costtype;
	public int costnumber;
	
	private static HashMap<Integer, BlackRefreshData> data =new HashMap<Integer, BlackRefreshData>();
	
	@Override
	public void addData()
	{
		data.put(number, this);
	}

	@Override
	public void parse(String[] ss)
	{
		
	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	public static BlackRefreshData getBlackRefreshData(int index)
	{
		return data.get(index);
	}

	public static Integer getDataSize()
	{
		return data.size();
	}
}
