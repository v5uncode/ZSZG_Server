package com.begamer.card.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.begamer.card.cache.Cache;
import com.begamer.card.cache.MailThread;
import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.Constant;
import com.begamer.card.common.util.GmSpecialMailUtil;
import com.begamer.card.common.util.Random;
import com.begamer.card.common.util.RequestUtil;
import com.begamer.card.common.util.Statics;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.common.util.binRead.AchievementData;
import com.begamer.card.common.util.binRead.AllThreeStarData;
import com.begamer.card.common.util.binRead.BagCostData;
import com.begamer.card.common.util.binRead.CardBoxData;
import com.begamer.card.common.util.binRead.CardData;
import com.begamer.card.common.util.binRead.CardExpData;
import com.begamer.card.common.util.binRead.CardGetData;
import com.begamer.card.common.util.binRead.CardkuData;
import com.begamer.card.common.util.binRead.ComposeData;
import com.begamer.card.common.util.binRead.EquipData;
import com.begamer.card.common.util.binRead.EquipupGradeData;
import com.begamer.card.common.util.binRead.EvolutionData;
import com.begamer.card.common.util.binRead.FirstBattleData;
import com.begamer.card.common.util.binRead.GameBoxData;
import com.begamer.card.common.util.binRead.ItemsData;
import com.begamer.card.common.util.binRead.MazeBattleData;
import com.begamer.card.common.util.binRead.MazeData;
import com.begamer.card.common.util.binRead.MazeProbabilityData;
import com.begamer.card.common.util.binRead.MazeSkillDropData;
import com.begamer.card.common.util.binRead.MissionData;
import com.begamer.card.common.util.binRead.PassiveSkillBasicExpData;
import com.begamer.card.common.util.binRead.PassiveSkillData;
import com.begamer.card.common.util.binRead.SackData;
import com.begamer.card.common.util.binRead.SkillData;
import com.begamer.card.common.util.binRead.SkillExpData;
import com.begamer.card.common.util.binRead.TakeCardTimeData;
import com.begamer.card.common.util.binRead.TaskData;
import com.begamer.card.common.util.binRead.UnLockData;
import com.begamer.card.common.util.binRead.UseableItemData;
import com.begamer.card.common.util.binRead.VipData;
import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.PackElement;
import com.begamer.card.json.SCEGJson;
import com.begamer.card.json.SCPSJson;
import com.begamer.card.json.command.IntensifyJson;
import com.begamer.card.json.command.IntensifyResultJson;
import com.begamer.card.json.command2.BattleJson;
import com.begamer.card.json.command2.BattleResultJson;
import com.begamer.card.json.command2.BreakJson;
import com.begamer.card.json.command2.BreakResultJson;
import com.begamer.card.json.command2.CGResultJson;
import com.begamer.card.json.command2.ComposeJson;
import com.begamer.card.json.command2.ComposeResultJson;
import com.begamer.card.json.command2.GameBoxJson;
import com.begamer.card.json.command2.GameBoxResultJson;
import com.begamer.card.json.command2.LotJson;
import com.begamer.card.json.command2.LotResultJson;
import com.begamer.card.json.command2.MapResultJson;
import com.begamer.card.json.command2.MazeBattleJson;
import com.begamer.card.json.command2.MazeBattleResultJson;
import com.begamer.card.json.command2.NewPlayerBattleResultJson;
import com.begamer.card.json.command2.SaleJson;
import com.begamer.card.json.command2.SaleResultJson;
import com.begamer.card.json.command2.SaveCGJson;
import com.begamer.card.json.command2.Star3RewardJson;
import com.begamer.card.json.command2.SweepCardJson;
import com.begamer.card.json.command2.SweepJson;
import com.begamer.card.json.command2.SweepResultJson;
import com.begamer.card.json.command2.SweepUiJson;
import com.begamer.card.json.command2.SweepUiResultJson;
import com.begamer.card.json.element.GameBoxElement;
import com.begamer.card.log.ErrorLogger;
import com.begamer.card.log.PlayerLogger;
import com.begamer.card.model.pojo.Activity;
import com.begamer.card.model.pojo.Announce;
import com.begamer.card.model.pojo.Card;
import com.begamer.card.model.pojo.CardGroup;
import com.begamer.card.model.pojo.Equip;
import com.begamer.card.model.pojo.Friend;
import com.begamer.card.model.pojo.GameBox;
import com.begamer.card.model.pojo.Item;
import com.begamer.card.model.pojo.LogBuy;
import com.begamer.card.model.pojo.LotRank;
import com.begamer.card.model.pojo.PassiveSkill;
import com.begamer.card.model.pojo.Player;
import com.begamer.card.model.pojo.Skill;

/**
 * 战前准备
 * 
 * @author DongHaina 2013-11-12 上午09:56:23
 */
public class BattleReadyController extends AbstractMultiActionController {
	private static final Logger logger = Logger.getLogger(BattleReadyController.class);
	private static Logger playlogger = PlayerLogger.logger;
	private static Logger errorlogger = ErrorLogger.logger;
	
	/** 客户端请求pve/pvp,服务器初始化战斗数据,发送给客户端 **/
	public ModelAndView initBattle(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			long time = System.currentTimeMillis();
			
			// 检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if (checkResult != null)
			{
				return checkResult;
			}
			// 获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			// 获取参数
			BattleJson bj = JSONObject.toJavaObject(jsonObject, BattleJson.class);
			ErrorJson ej = null;
			String msg = "";
			if (bj != null)
			{
				PlayerInfo pi = Cache.getInstance().getPlayerInfo(bj.getPlayerId(), request.getSession());
				int errorCode = 0;
				if (pi != null)
				{
					MissionData md = MissionData.getMissionData(bj.td);
					int sequence = 0;
					if (md != null)
					{
						sequence = md.getSequence();
					}
					// 判断是否需要新手引导,如果需要，则判断第几次战斗
					if (pi.player.getNewPlayerType() == 0)// 新手引导--战斗演示
					{
						ej = new NewPlayerBattleResultJson();
						NewPlayerBattleResultJson nbrj = (NewPlayerBattleResultJson) ej;
						FirstBattleData fbData1 = FirstBattleData.getFirstBattleDataByTeam(1);// 我方
						FirstBattleData fbData2 = FirstBattleData.getFirstBattleDataByTeam(2);// 敌方
						nbrj.cs0 = FirstBattleData.getCs(1);
						nbrj.cs1 = FirstBattleData.getCs(2);
						nbrj.us0 = fbData1.uniteskill;
						nbrj.us1 = fbData2.uniteskill;
						int[] maxEnergys = { pi.player.getMaxEnergy(), 1000 };
						nbrj.mes = maxEnergys;
						nbrj.initE = VipData.getInitEnergy(pi.player.getVipLevel());
						msg = JSON.toJSONString(nbrj);
						playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|进入新手引导--战斗演示");
					}
					else
					{
						ej = new BattleResultJson();
						BattleResultJson brj = (BattleResultJson) ej;
						// 是否挑战新解锁的关卡
						boolean isNewUnlock = false;
						// 校验阶段
						int td = bj.td;
						MissionData mData = MissionData.getMissionData(td);
						if (mData == null)
						{
							errorlogger.error("td:" + td);
						}
						if (mData.missiontype == 1)// 普通本
						{
							MissionData md1 = MissionData.getUnlockData(pi.player.getMissionId());
							if (md1 != null)
							{
								if (td > md1.id)
								{
									errorCode = 13;
								}
								else if (td == md1.id)
								{
									isNewUnlock = true;
								}
							}
						}
						else if (mData.missiontype == 2)// 精英本
						{
							MissionData md2 = MissionData.getUnlockData(pi.player.getMissionId2());
							if (md2 != null)
							{
								if (td > md2.id)
								{
									errorCode = 13;
								}
								else
								{
									if (td == md2.id)
									{
										isNewUnlock = true;
									}
									// 判断map
									MissionData md1 = MissionData.getUnlockData(pi.player.getMissionId());
									if (md1 != null)
									{
										if (mData.map > md1.map)
										{
											errorCode = 14;
										}
									}
								}
							}
						}
						CardGroup cg = pi.getCardGroup();
						String[] ds = null;
						//
						if (pi.player.getNewPlayerType() == 8 && td == 110104)
						{
							int t = 0;
							for (int i = 0; i < pi.getCards().size(); i++)
							{
								if (pi.getCards().get(i).getCardId() == 14001)
								{
									t = 1;
									break;
								}
							}
							if (t == 0)
							{
								pi.addCard(14001, 1);// 如果此时玩家没有孙悟空。则给其一张
							}
							ds = new String[1];
							ds[0] = 3 + "-" + 21002 + "," + 1;// 新手固定掉落雪女
						}
						else
						// 正常掉落，掉落物品随机
						{
							ds = Statics.dropsThings(bj.td, cg, pi);
						}
						// 校验出战卡组是否有卡
						if (errorCode == 0)
						{
							if (!cg.canFight())
							{
								errorCode = 65;
							}
						}
						if (errorCode == 0)// 校验背包容量
						{
							if (ds != null && ds.length > 0)
							{
								if (!Statics.checkPackage(ds, pi))
								{
									errorCode = 53;
									BagCostData bd = BagCostData.getData(pi.player.getBuyBagTimes() + 1);
									if (bd == null)
									{
										errorCode = 131;
									}
								}
							}
						}
						// 判断挑战次数
						if (errorCode == 0)
						{
							if (md.missiontype == 1)
							{
								String times = pi.player.getTimes1();
								int beginIndex = (sequence - 1) * 2;
								int endIndex = beginIndex + 2;
								int num = StringUtil.getInt(times.substring(beginIndex, endIndex));
								if (num >= md.times)
								{
									errorCode = 50;
								}
							}
							else
							{
								String times = pi.player.getTimes2();
								int beginIndex = (sequence - 1) * 2;
								int endIndex = beginIndex + 2;
								int num = StringUtil.getInt(times.substring(beginIndex, endIndex));
								if (num >= md.times)
								{
									errorCode = 50;
								}
							}
						}
						if (errorCode == 0)
						{// 校验体力
							if (pi.player.getPower() < mData.cost)
							{
								errorCode = 27;
							}
						}
						if (errorCode == 0)
						{
							// 邀请好友免费,邀请陌生人需要花费金币
							if (bj.h != 0 && !pi.isFriend(bj.h))
							{
								// 判断金币是否足够
								int need = Statics.getNextInviteFriendCost(pi.player.getLevel(), pi.player.getInviteFriendTimes());
								if (pi.player.getGold() < need)
								{
									errorCode = 89;
								}
							}
						}
						// 功能阶段
						if (errorCode == 0)
						{
							brj.md = bj.td;
							brj.cs0 = cg.getBattleData();
							brj.cs1 = MissionData.getMonsterData(bj.td);
							// 生成掉落物品
							brj.ds = ds;
							String str = "";
							if (mData.missiontype == 1)
							{
								str = pi.player.getBouns1();
							}
							else if (mData.missiontype == 2)
							{
								str = pi.player.getBouns2();
							}
							// 判断bonus标识
							int suqence = mData.getSequence();
							if (str != null && str.length() > 0)
							{
								if (suqence > str.length())
								{
									brj.bs = 0;
								}
								else
								{
									if (str.substring(suqence - 1, suqence) != null && str.substring(suqence - 1, suqence).length() > 0)
									{
										brj.bs = StringUtil.getInt(str.substring(suqence - 1, suqence));
									}
									else
									{
										brj.bs = 0;
									}
								}
							}
							else
							{
								brj.bs = 0;
							}
							// 双方合体技
							int helperId = bj.h;
							int addFriendValue = 0;
							int[] us0 = new int[3];
							us0[0] = cg.unitId;
							PlayerInfo friendPi = Cache.getInstance().getPlayerInfo(helperId);
							if (friendPi == null)
							{
								us0[2] = 0;
							}
							else
							{
								if (!pi.isFriend(helperId))
								{
									int need = Statics.getNextInviteFriendCost(pi.player.getLevel(), pi.player.getInviteFriendTimes());
									pi.player.removeGold(need);
									pi.player.addInviteTimes();
									addFriendValue = 10;
									pi.needAddHelperAsFriend = true;
									// 如果已经发出申请,则不再申请
									for (Friend apply : Cache.getInstance().getMyApplys(pi.player.getId()))
									{
										if (apply.getFriendId() == helperId)
										{
											pi.needAddHelperAsFriend = false;
										}
									}
									// 设置申请
									if (pi.needAddHelperAsFriend)
									{
										pi.helperPlayerId = helperId;
										pi.helperPlayerName = friendPi.player.getName();
									}
								}
								else
								{
									addFriendValue = 25;
									pi.needAddHelperAsFriend = false;
									Friend f = pi.getFriend(helperId);
									f.setLastHelpDay(StringUtil.getDate(System.currentTimeMillis()));
								}
								brj.fs = friendPi.getCardGroup().getBattleData();
								brj.fr = friendPi.player.getRuneId();
								us0[2] = friendPi.player.getIconId();// 选取好友合体技
							}
							pi.player.addFriendValue(addFriendValue);
							brj.us0 = us0;
							brj.bNum = pi.bNum + 1;
							brj.aF = addFriendValue;
							if (isNewUnlock && TaskData.haveTask(td))
							{
								brj.t = 1;
							}
							int[] maxEnergys = { pi.player.getMaxEnergy(), 0 };
							brj.mes = maxEnergys;
							brj.initE = VipData.getInitEnergy(pi.player.getVipLevel());
							// 记录战斗信息
							pi.brj = brj;
							// 记录最后的missionId
							pi.player.setLastMissionId(bj.td);
							// 己方种族加成属性
							brj.raceAtts = Statics.getRaceAttris(pi);
							msg = JSON.toJSONString(brj);
							playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|进入推图|" + md.id);
						}
						else
						{
							ej = new ErrorJson();
							ej.errorCode = errorCode;
						}
					}
				}
				else
				{
					ej = new ErrorJson();
					ej.errorCode = -3;
				}
			}
			else
			{
				ej = new ErrorJson();
				ej.errorCode = -1;
			}
			Cache.recordRequestNum(ej);
			msg = JSON.toJSONString(ej);
			logger.info("msg:" + msg);
			RequestUtil.setResponseMsg(request, msg);
			
			logger.info("初始化pve耗时:" + (System.currentTimeMillis() - time) + "ms");
			
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e)
		{
			errorlogger.error(this.getClass().getName() + "->" + Thread.currentThread().getStackTrace()[1].getMethodName() + "():", e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
	
	/** 物品出售 **/
	public ModelAndView sale(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			// 检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if (checkResult != null)
			{
				return checkResult;
			}
			// 获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			// 获取参数
			SaleJson sj = JSONObject.toJavaObject(jsonObject, SaleJson.class);
			SaleResultJson srj = new SaleResultJson();
			if (sj != null)
			{
				PlayerInfo pi = Cache.getInstance().getPlayerInfo(sj.getPlayerId(), request.getSession());
				if (pi != null)
				{
					int addGold = 0;
					// 校验正确
					int errorCode = 0;
					if (sj.list != null)
					{
						/** 1card,2skill,8passiveSkill,3equip,4item **/
						switch (sj.type)
						{
							case 1:// card
								if (sj.list != null)
								{
									List<Card> cards = pi.getCards();
									// 校验阶段
									for (int i : sj.list)
									{
										if (i >= cards.size() || i < 0)
										{
											errorCode = 6;
											break;
										}
										// 循环卡组信息判断销售的物品是否在卡组中
										if (Statics.isInCardGroup(pi, 1, i))
										{
											errorCode = 5;
											break;
										}
									}
									// 功能阶段
									if (errorCode == 0)
									{
										for (int i : sj.list)
										{
											playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|出售角色卡:" + cards.get(i).getCardId());
											cards.get(i).setSell(1);
											Card card = cards.get(i);
											CardData cd = CardData.getData(card.getCardId());
											addGold += cd.sell * card.getLevel();
										}
									}
								}
								break;
							case 2:// skill
								if (sj.list != null)
								{
									List<Skill> skills = pi.getSkills();
									// 校验阶段
									for (int i : sj.list)
									{
										if (i >= skills.size() || i < 0)
										{
											errorCode = 6;
											break;
										}
										if (Statics.isInCardGroup(pi, 2, i))
										{
											errorCode = 5;
											break;
										}
									}
									// 功能阶段
									if (errorCode == 0)
									{
										for (int i : sj.list)
										{
											playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|出售skill卡:" + skills.get(i).getSkillId());
											skills.get(i).setSell(1);
											SkillData sd = SkillData.getData(skills.get(i).getSkillId());
											addGold += sd.sell;
										}
									}
								}
								break;
							case 3:// equip
								if (sj.list != null)
								{
									List<Equip> equips = pi.getEquips();
									// 校验
									for (int i : sj.list)
									{
										if (i >= equips.size() || i < 0)
										{
											errorCode = 6;
											break;
										}
										if (Statics.isInCardGroup(pi, 4, i))
										{
											errorCode = 5;
											break;
										}
									}
									// 功能
									if (errorCode == 0)
									{
										for (int i : sj.list)
										{
											playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|出售equip卡:" + equips.get(i).getEquipId());
											equips.get(i).setSell(1);
											EquipData eData = EquipData.getData(equips.get(i).getEquipId());
											EquipupGradeData ed = EquipupGradeData.getData(equips.get(i).getLevel());
											addGold = addGold + ed.sell + eData.sell;
										}
									}
								}
								break;
							case 4:// item
								if (sj.list != null)
								{
									List<Item> items = pi.getItems();
									for (int i : sj.list)
									{
										if (i >= items.size() || i < 0)
										{
											errorCode = 6;
											break;
										}
									}
									if (errorCode == 0)
									{
										for (int i : sj.list)
										{
											ItemsData ed = ItemsData.getItemsData(items.get(i).getItemId());
											addGold += ed.sell * items.get(i).getPile();
											playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|出售item卡:" + items.get(i).getItemId());
											items.get(i).setSell(1);
										}
									}
								}
								break;
							case 8:// passiveSkill
								if (sj.list != null)
								{
									List<PassiveSkill> passiveSkills = pi.getPassiveSkillls();
									// 校验
									for (int i : sj.list)
									{
										if (i >= passiveSkills.size() || i < 0)
										{
											errorCode = 6;
											break;
										}
										if (Statics.isInCardGroup(pi, 3, i))
										{
											errorCode = 5;
											break;
										}
									}
									// 功能阶段
									if (errorCode == 0)
									{
										for (int i : sj.list)
										{
											playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|出售ps卡:" + passiveSkills.get(i).getPassiveSkillId());
											passiveSkills.get(i).setSell(1);
											PassiveSkillData psd = PassiveSkillData.getData(passiveSkills.get(i).getPassiveSkillId());
											addGold += psd.sell;
										}
									}
								}
								break;
						}
					}
					if (errorCode == 0)
					{
						pi.player.addGold(addGold);
						srj.g = pi.player.getGold();
						srj.addG = addGold;
						playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|出售物品");
					}
					else
					{
						srj.errorCode = errorCode;
					}
					
				}
				else
				{
					srj.errorCode = -3;
				}
			}
			else
			{
				srj.errorCode = -1;
			}
			Cache.recordRequestNum(srj);
			String msg = JSON.toJSONString(srj);
			logger.info("msg:" + msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e)
		{
			errorlogger.error(this.getClass().getName() + "->" + Thread.currentThread().getStackTrace()[1].getMethodName() + "():", e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
	
	/** 角色卡、技能卡强化 **/
	public ModelAndView intensify(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			// 检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if (checkResult != null)
			{
				return checkResult;
			}
			// 获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/*********************************** 修改部分 start ************************************/
			// 获取参数
			IntensifyJson ij = JSONObject.toJavaObject(jsonObject, IntensifyJson.class);
			IntensifyResultJson irj = new IntensifyResultJson();
			if (ij != null)
			{
				// 获取请求的数据
				PlayerInfo pi = Cache.getInstance().getPlayerInfo(ij.getPlayerId(), request.getSession());
				if (pi != null)
				{
					// 校验正确
					boolean b = true;
					switch (ij.type)
					{
						case 1:// card
							List<Card> cards = pi.getCards();
							if (ij.index < 0 || ij.index >= cards.size())
							{
								irj.errorCode = 4;
							}
							else
							{
								Card c = cards.get(ij.index);
								CardData cData = CardData.getData(c.getCardId());
								CardExpData ceData = CardExpData.getData(c.getLevel());
								// 计算被吞噬者的总经验
								int exp = 0;
								int gold = ceData.cost * ij.list.size();
								
								// 如果新手，校验目标卡是否为孙悟空
								if (pi.player.getNewPlayerType() == 8)
								{
									if (c.getCardId() != 14001)
									{
										irj.errorCode = 62;
										b = false;
									}
								}
								
								// 校验金币
								if (pi.player.getGold() < gold)
								{
									irj.errorCode = 10;
									b = false;
								}
								// 校验是否为经验卡，突破卡和金币卡（不能强化）
								if (b)
								{
									if (cData.race == 5 || cData.race == 6 || cData.race == 7)
									{
										irj.errorCode = 16;
										b = false;
									}
								}
								// 校验卡组
								if (b)
								{
									for (int i : ij.list)
									{
										if (i < 0 || i >= cards.size())
										{
											b = false;
											irj.errorCode = 6;
											break;
										}
										if (i == ij.index)
										{
											b = false;
											irj.errorCode = 15;
										}
										// 循环卡组信息判断被强化的物品是否在卡组中,在卡组中为true
										if (Statics.isInCardGroup(pi, 1, i))
										{
											b = false;
											irj.errorCode = 5;
											break;
										}
										CardData cardData = CardData.getData(cards.get(i).getCardId());
										if (cardData.race == 6 || cardData.race == 7)
										{
											irj.errorCode = 17;
											b = false;
											break;
										}
									}
								}
								if (b)
								{
									if (c.getBreakNum() > 0)
									{
										EvolutionData evolutionData = EvolutionData.getData(cData.star, c.getBreakNum());
										if (c.getLevel() >= cData.maxLevel + evolutionData.lvl)
										{
											b = false;
											irj.errorCode = 11;
										}
									}
									else
									{
										if (c.getLevel() >= cData.maxLevel)
										{
											b = false;
											irj.errorCode = 11;
										}
									}
								}
								// 功能阶段
								if (b)
								{
									// 记录每日任务进度
									ActivityController.updateTaskComplete(5, pi, 1);
									pi.player.addActive(5, 1);
									// 计算经验值
									for (int i : ij.list)
									{
										Card cTemp = cards.get(i);
										CardData cardData = CardData.getData(cTemp.getCardId());
										if (cardData.race == cData.race)
										{
											exp = exp + (int) (cardData.exp * cTemp.getLevel() * 1.5);
										}
										else
										{
											exp = exp + cardData.exp * cTemp.getLevel();
										}
										
										cTemp.setSell(2);
										playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|强化消耗角色卡:" + cTemp.getCardId());
									}
									c.addExp(exp);
									pi.player.removeGold(gold);
									irj.pe = Statics.createPackElement(pi.getCards().indexOf(c), c, pi);
									// 如果新手初次强化，改变数据库新手标识
									if (pi.player.getNewPlayerType() == 8)
									{
										pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
									}
									playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|强化角色卡:" + c.getCardId());
								}
							}
							break;
						case 2:// skill 校验skill时state用0表示，passiveskill时state用1表示
							List<Skill> skills = pi.getSkills();
							if (ij.index < 0 || ij.index >= skills.size())
							{
								irj.errorCode = 4;
							}
							else
							{
								Skill s = skills.get(ij.index);
								SkillData sData = SkillData.getData(s.getSkillId());
								SkillExpData seData = SkillExpData.getData(s.getLevel());
								// new 花费金币
								int gold = seData.cost * ij.list.size();
								// 校验强化的卡是否满级或者经验卡
								if (s.getLevel() >= pi.player.getLevel() || sData.exptype == 2)
								{
									irj.errorCode = 16;
									b = false;
								}
								
								// 校验阶段
								for (int i : ij.list)
								{
									if (i < 0 || i >= skills.size())
									{
										b = false;
										irj.errorCode = 6;
										break;
									}
									if (i == ij.index)
									{
										b = false;
										irj.errorCode = 15;
									}
									if (Statics.isInCardGroup(pi, 2, i))
									{
										b = false;
										irj.errorCode = 5;
										break;
									}
								}
								if (b)// 校验金币
								{
									if (pi.player.getGold() < gold)
									{
										b = false;
										irj.errorCode = 19;
									}
								}
								// 功能阶段
								if (b)
								{
									// 记录每日任务进度
									ActivityController.updateTaskComplete(5, pi, 1);
									pi.player.addActive(5, 1);
									int skillExp = 0;
									for (int i : ij.list)
									{
										Skill sTemp = skills.get(i);
										SkillData skillData = SkillData.getData(sTemp.getSkillId());
										skillExp = skillData.exp + skillExp + sTemp.getCurExp();
										for (int k = 0; k < sTemp.getLevel(); k++)
										{
											SkillExpData skillExpData = SkillExpData.getData(k + 1);
											
											if (skillExpData != null)
											{
												skillExp = skillExp + skillExpData.starexps[skillData.star - 1];
											}
										}
										sTemp.setSell(2);
										playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|强化消耗skill卡:" + sTemp.getSkillId());
									}
									s.addCurExp(skillExp, pi.player.getLevel());
									pi.player.removeGold(gold);
									irj.pe = Statics.createPackElement(pi.getSkills().indexOf(s), s, pi);
									playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|强化技能:" + s.getSkillId());
								}
							}
							break;
						case 4:// equip
							Equip e = pi.getEquips().get(ij.index);// 强化的装备
							// 校验
							EquipupGradeData egData = EquipupGradeData.getData(e.getLevel());
							Player player = pi.player;
							if (ij.numType == 1)
							{
								if (b)
								{
									if (e.getLevel() >= player.getLevel() * 3)
									{
										irj.errorCode = 12;
										b = false;
									}
									else if (player.getGold() < egData.cost)
									{
										irj.errorCode = 10;
										b = false;
									}
								}
								// 功能
								if (b)
								{
									// 记录每日任务进度
									ActivityController.updateTaskComplete(5, pi, 1);
									pi.player.addActive(5, 1);
									int num1 = Random.getNumber(1, 1000);
									if (num1 > 0 && num1 <= egData.probability1)
									{
										e.setLevel(e.getLevel() + 1);
										irj.state = 0;
										playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|强化装备:" + e.getEquipId() + "|等级+1|现在等级:" + e.getLevel());
									}
									else if (num1 > egData.probability1 && num1 <= (egData.probability3 + egData.probability1))
									{
										// 暴击
										e.setLevel(e.getLevel() + 3);
										irj.state = 1;
										playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|强化装备:" + e.getEquipId() + "|等级+3|现在等级:" + e.getLevel());
									}
									else
									{
										// 强化失败
										irj.state = -1;
										playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|强化装备:" + e.getEquipId() + "|失败|现在等级:" + e.getLevel());
									}
									player.removeGold(egData.cost);
									irj.pe = Statics.createPackElement(pi.getEquips().indexOf(e), e, pi);
									// 新手装备强化
									if (pi.player.getNewPlayerType() == 14)
									{
										pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
									}
								}
							}
							else if (ij.numType > 1)
							{
								if (pi.player.getVipLevel() == 0)
								{
									irj.errorCode = 70;
									b = false;
								}
								else if (pi.player.getVipLevel() >= 1)
								{
									VipData vData = VipData.getVipData(pi.player.getVipLevel());
									if (vData.autoupgrade == 0)
									{
										irj.errorCode = 70;
										b = false;
									}
								}
								if (b)
								{
									if (e.getLevel() >= player.getLevel() * 3)
									{
										irj.errorCode = 12;
										b = false;
									}
									else if (player.getGold() < egData.cost)
									{
										irj.errorCode = 10;
										b = false;
									}
								}
								if (b)
								{
									for (int k = 0; k < ij.numType; k++)
									{
										ActivityController.updateTaskComplete(5, pi, 1);
										pi.player.addActive(5, 1);
										int num1 = Random.getNumber(1, 1000);
										if (num1 > 0 && num1 <= egData.probability1)
										{
											e.setLevel(e.getLevel() + 1);
											playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|强化装备:" + e.getEquipId() + "|等级+1|现在等级:" + e.getLevel());
										}
										else if (num1 > egData.probability1 && num1 <= (egData.probability3 + egData.probability1))
										{
											// 暴击
											e.setLevel(e.getLevel() + 3);
											playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|强化装备:" + e.getEquipId() + "|等级+3|现在等级:" + e.getLevel());
										}
										irj.state = 0;
										player.removeGold(egData.cost);
										irj.pe = Statics.createPackElement(pi.getEquips().indexOf(e), e, pi);
										if (e.getLevel() >= player.getLevel() * 3)
										{
											break;
										}
										else if (player.getGold() < egData.cost)
										{
											break;
										}
									}
								}
							}
							break;
						case 3:// passiveskill
							List<PassiveSkill> pskills = pi.getPassiveSkillls();
							if (ij.index < 0 || ij.index >= pskills.size())
							{
								irj.errorCode = 4;
							}
							else
							{
								PassiveSkill ps = pskills.get(ij.index);
								PassiveSkillData psData = PassiveSkillData.getData(ps.getPassiveSkillId());
								// 校验强化的卡是否满级
								if (psData.level == 10)
								{
									irj.errorCode = 16;
									b = false;
								}
								int pskillbasicExp = 0;
								// 校验阶段
								for (int i : ij.list)
								{
									if (i < 0 || i >= pskills.size())
									{
										b = false;
										irj.errorCode = 6;
										break;
									}
									if (i == ij.index)
									{
										b = false;
										irj.errorCode = 15;
									}
									if (Statics.isInCardGroup(pi, 3, i))
									{
										b = false;
										irj.errorCode = 5;
										break;
									}
								}
								// 功能阶段
								if (b)
								{
									// 记录每日任务进度
									ActivityController.updateTaskComplete(5, pi, 1);
									pi.player.addActive(5, 1);
									for (int i : ij.list)
									{
										PassiveSkill psTemp = pskills.get(i);
										PassiveSkillData passiveSkillData = PassiveSkillData.getData(psTemp.getPassiveSkillId());
										PassiveSkillBasicExpData pSkillBasicExpData = PassiveSkillBasicExpData.getData(passiveSkillData.star);
										for (int k = 0; k < passiveSkillData.level; k++)
										{
											PassiveSkillData passiveSkillData2 = PassiveSkillData.getData(psTemp.getPassiveSkillId() - k);
											pskillbasicExp = pskillbasicExp + passiveSkillData2.exp;
										}
										pskillbasicExp = pskillbasicExp + pSkillBasicExpData.basicexp;
										psTemp.setSell(2);
										playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|强化消耗ps卡:" + psTemp.getPassiveSkillId());
									}
									ps.addCurExp(pskillbasicExp);
									irj.pe = Statics.createPackElement(pi.getPassiveSkillls().indexOf(ps), ps, pi);
									playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|强化被动技能:" + ps.getPassiveSkillId());
								}
							}
							break;
					}
				}
				else
				{
					irj.errorCode = -3;
				}
			}
			else
			{
				irj.errorCode = -1;
			}
			Cache.recordRequestNum(irj);
			String msg = JSON.toJSONString(irj);
			/*********************************** 修改部分 end **************************************/
			logger.info("msg:" + msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e)
		{
			errorlogger.error(this.getClass().getName() + "->" + Thread.currentThread().getStackTrace()[1].getMethodName() + "():", e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
	
	/** 保存卡组 **/
	public ModelAndView saveFormation(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			// 检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if (checkResult != null)
			{
				return checkResult;
			}
			// 获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/*********************************** 修改部分 start ************************************/
			// 获取参数
			SaveCGJson scgj = JSONObject.toJavaObject(jsonObject, SaveCGJson.class);
			ErrorJson ej = null;
			String msg = "";
			if (scgj != null)
			{
				// 获取请求的数据
				PlayerInfo pi = Cache.getInstance().getPlayerInfo(scgj.getPlayerId(), request.getSession());
				if (pi != null)
				{
					ej = new CGResultJson();
					CGResultJson cgj = (CGResultJson) ej;
					boolean b = true;
					CardGroup cg = pi.getCardGroup();
					
					if (b)
					{
						if (scgj.ics == null || scgj.ics.length != 6)
						{
							cgj.errorCode = 8;
							b = false;
						}
						else
						{
							List<Integer> cardIds = new ArrayList<Integer>();
							List<Card> cards = pi.getCards();
							for (int k : scgj.ics)
							{
								if (k >= cards.size())
								{
									cgj.errorCode = 8;
									b = false;
									break;
								}
								else
								{
									if (k >= 0)
									{
										int cardId = cards.get(k).getCardId();
										CardData cd = CardData.getData(cardId);
										if (cd == null || cd.race > 4 || cardIds.contains(cardId))
										{
											cgj.errorCode = 8;
											b = false;
											break;
										}
										else
										{
											cardIds.add(cardId);
										}
									}
								}
							}
						}
					}
					if (b)
					{
						if (scgj.iss == null || scgj.iss.length != 6)
						{
							cgj.errorCode = 8;
							b = false;
						}
						else
						{
							List<Skill> skills = pi.getSkills();
							for (int k : scgj.iss)
							{
								if (k >= skills.size())
								{
									cgj.errorCode = 8;
									b = false;
									break;
								}
							}
						}
					}
					if (b)
					{
						if (scgj.ips == null || scgj.ips.length != 6)
						{
							cgj.errorCode = 8;
							b = false;
						}
						else
						{
							List<PassiveSkill> pSkills = pi.getPassiveSkillls();
							for (SCPSJson k : scgj.ips)
							{
								if (k != null && b)
								{
									for (int m : k.ps)
									{
										if (m >= pSkills.size())
										{
											cgj.errorCode = 8;
											b = false;
											break;
										}
										if (m >= 0 && m < pSkills.size())
										{
											PassiveSkill ps = pSkills.get(m);
											PassiveSkillData psd = PassiveSkillData.getData(ps.getPassiveSkillId());
											for (int j : k.ps)
											{
												if (j != m && j >= 0 && j < pSkills.size())
												{
													PassiveSkill ps2 = pSkills.get(j);
													PassiveSkillData psd2 = PassiveSkillData.getData(ps2.getPassiveSkillId());
													if (psd.type == psd2.type)
													{
														cgj.errorCode = 8;
														b = false;
														break;
													}
												}
											}
										}
									}
									List<Integer> m = k.ps;
									for (int i = 0; i < 3; i++)
									{
										int num = m.get(i);
										if (i == 0)
										{
											if (num > -1 && !UiController.checkMode(34, pi))
											{
												cgj.errorCode = 119;
												b = false;
												break;
											}
										}
										if (i == 1)
										{
											if (num > -1 && !UiController.checkMode(35, pi))
											{
												cgj.errorCode = 119;
												b = false;
												break;
											}
										}
										if (i == 2)
										{
											if (num > -1 && !UiController.checkMode(36, pi))
											{
												cgj.errorCode = 119;
												b = false;
												break;
											}
										}
									}
								}
							}
						}
					}
					if (b)
					{
						if (scgj.ies == null || scgj.ies.length != 6)
						{
							cgj.errorCode = 8;
							b = false;
						}
						else
						{
							List<Equip> equips = pi.getEquips();
							for (SCEGJson k : scgj.ies)
							{
								if (k != null && b)
								{
									for (int m : k.es)
									{
										if (m >= equips.size())
										{
											cgj.errorCode = 8;
											b = false;
											break;
										}
									}
								}
							}
						}
					}
					if (b)
					{
						// 判断当前合体技是否解锁
						int unit = scgj.us;
						String unitIds = pi.player.getUnitskills();
						if (!unitIds.contains(unit + "") && unit != 0)
						{
							cgj.errorCode = 8;
							b = false;
						}
					}
					// 判断解锁
					if (b)
					{
						int cardNum = 0;
						for (int ic : scgj.ics)
						{
							if (ic > -1)
							{
								cardNum++;
							}
						}
						if (!((cardNum == 1 && UiController.checkMode(27, pi)) || (cardNum == 2 && UiController.checkMode(28, pi)) || (cardNum == 3 && UiController.checkMode(29, pi)) || (cardNum == 4 && UiController.checkMode(30, pi)) || (cardNum == 5 && UiController.checkMode(31, pi)) || (cardNum == 6 && UiController.checkMode(32, pi))))
						{
							cgj.errorCode = 56;
							b = false;
						}
					}
					if (b)
					{
						// 保存卡组
						cg.save(pi, scgj.ics, scgj.iss, scgj.ips, scgj.ies, scgj.us);
						// 取得目标卡组
						cgj.transform(cg, pi);
						// 新手改变数据库标识
						if (pi.player.getNewPlayerType() == 2)// 新手阵容--孙悟空上阵
						{
							pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
						}
						else if (pi.player.getNewPlayerType() == 3)// 新手--孙悟空上阵之后使用合体技
						{
							pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
						}
						else if (pi.player.getNewPlayerType() == 13 && pi.player.getMissionId() == 110203)// 新手装备教学
						{
							pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
						}
						else if (pi.player.getNewPlayerType() == 18)// 新手主动技能教学
						{
							pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
						}
						else if (pi.player.getNewPlayerType() == 7)// 新手齐格飞上阵
						{
							pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
						}
						playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|保存卡组");
					}
					msg = JSON.toJSONString(cgj);
				}
				else
				{
					ej = new ErrorJson();
					ej.errorCode = -3;
					msg = JSON.toJSONString(ej);
				}
			}
			else
			{
				ej = new ErrorJson();
				ej.errorCode = -1;
				msg = JSON.toJSONString(ej);
			}
			Cache.recordRequestNum(ej);
			/*********************************** 修改部分 end **************************************/
			logger.info("msg:" + msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e)
		{
			errorlogger.error(this.getClass().getName() + "->" + Thread.currentThread().getStackTrace()[1].getMethodName() + "():", e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
	
	/** 合成 **/
	public ModelAndView compose(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			// 检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if (checkResult != null)
			{
				return checkResult;
			}
			// 获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/*********************************** 修改部分 start ************************************/
			// 获取参数
			ComposeJson cj = JSONObject.toJavaObject(jsonObject, ComposeJson.class);
			ComposeResultJson crj = new ComposeResultJson();
			if (cj != null)
			{
				PlayerInfo pi = Cache.getInstance().getPlayerInfo(cj.getPlayerId(), request.getSession());
				if (pi != null)
				{
					int errorcode = 0;
					// 校验阶段
					ComposeData comData = ComposeData.getComDataByComposite(cj.index, cj.type);
					if (comData != null)
					{
						List<String> material_num = comData.material_num;
						// 校验解锁
						if (comData.style == 1)
						{
							if (comData.unlockmission > pi.player.getLevel())
							{
								errorcode = 22;
							}
						}
						else if (comData.style == 2)
						{
							if (comData.unlockmission > pi.player.getMissionId())
							{
								errorcode = 22;
							}
						}
						else if (comData.style == 3)
						{
							if (material_num != null && material_num.size() > 0)
							{
								for (int i = 0; i < material_num.size(); i++)
								{
									String[] str = material_num.get(i).split("-");
									List<Item> items = pi.getTargetItems(StringUtil.getInt(str[0]));
									if (items == null || items.size() == 0)
									{
										errorcode = 22;
										break;
									}
								}
							}
						}
						if (errorcode == 0)
						{
							if (material_num != null && material_num.size() > 0)
							{
								// 校验材料数量是否足够
								for (int i = 0; i < material_num.size(); i++)
								{
									String[] str = material_num.get(i).split("-");
									List<Item> items = pi.getTargetItems(StringUtil.getInt(str[0]));
									if (items != null && items.size() > 0)
									{
										int pile = pi.getTargetItemsNum(StringUtil.getInt(str[0]));
										if (pile < StringUtil.getInt(str[1]))
										{
											errorcode = 20;
											break;
										}
									}
									else
									{
										errorcode = 20;
										break;
									}
								}
							}
						}
					}
					else
					{
						errorcode = 21;
					}
					// 校验背包容量
					if (errorcode == 0)
					{
						int packageNum = Statics.getPackageNum(pi);
						if (cj.type == 1 && pi.getCards().size() >= packageNum)
						{
							errorcode = 53;
						}
						else if (cj.type == 2 && pi.getEquips().size() >= packageNum)
						{
							errorcode = 53;
						}
						else if (cj.type == 3 && pi.getItems().size() >= packageNum)
						{
							errorcode = 53;
						}
						else if (cj.type == 4 && pi.getSkills().size() >= packageNum)
						{
							errorcode = 53;
						}
					}
					if (errorcode == 0)
					{
						if (pi.player.getGold() < comData.cost)
						{
							errorcode = 19;
						}
					}
					// 功能阶段
					if (errorcode == 0)
					{
						List<Integer> cardIds = new ArrayList<Integer>();
						List<String> list = comData.material_num;
						for (int i = 0; i < list.size(); i++)
						{
							String[] str = list.get(i).split("-");
							// List<Item> its
							// =pi.getTargetItems(StringUtil.getInt(str[0]));
							int pile = StringUtil.getInt(str[1]);
							playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|合成消耗iitem卡:" + str[0]);
							pi.removeItem(StringUtil.getInt(str[0]), pile);
						}
						int num = Random.getNumber(0, 1000);
						// 合成成功
						if (num >= 0 && num <= comData.probability)
						{
							switch (cj.type)
							{
								case 1:// card
									pi.addCard(cj.index, 1);
									cardIds.add(cj.index);
									break;
								case 2:// equip
									pi.addEquip(cj.index);
									pi.player.setComposeEquipTimes(pi.player.getComposeEquipTimes() + 1);
									// 更新成就
									pi.player.updateAchieve(15, pi.player.getComposeEquipTimes() + "");
									break;
								case 3:// item
									pi.addItem(cj.index, 1);
									break;
								case 4:// skill
									pi.addSkill(cj.index, 1);
							}
						}
						// 合成失败
						else
						{
							crj.t = -1;
						}
						if (cardIds.size() > 0)
						{
							pi.getNewUnitSkill(cardIds);
						}
						// 金币消耗(新手不消耗)
						if (pi.player.getNewPlayerType() > 16)
						{
							pi.player.removeGold(comData.cost);
						}
						if (pi.player.getNewPlayerType() == 16)// 新手合成
						{
							pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
						}
						crj.pes = UiController.composeMaterial(cj.index, cj.type, pi);
						playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|合成物品");
						crj.pes = UiController.composeMaterial(cj.index, cj.type, pi);
						// 如果转盘在活动期间。则根据合成卡牌的星级赠送给玩家抽奖次数
						if (cj.type == 1)
						{
							CardData cData = CardData.getData(cj.index);
							Activity a = Cache.getInstance().getActivityById(8);
							if (a.getSttime() != null && a.getEndtime() != null && a.getSttime().length() > 0 && a.getEndtime().length() > 0)
							{
								if (StringUtil.getDateTime(System.currentTimeMillis()).compareTo(a.getEndtime()) > 0 && StringUtil.getDateTime(System.currentTimeMillis()).compareTo(a.getSttime()) > 0)
								{
									pi.player.addLottoTimes(3, cData.star);
								}
							}
						}
						
						playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|合成物品");
					}
					else
					{
						crj.errorCode = errorcode;
					}
				}
				else
				{
					crj.errorCode = -3;
				}
			}
			else
			{
				crj.errorCode = -1;
			}
			Cache.recordRequestNum(crj);
			String msg = JSON.toJSONString(crj);
			/*********************************** 修改部分 end **************************************/
			logger.info("msg:" + msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e)
		{
			errorlogger.error(this.getClass().getName() + "->" + Thread.currentThread().getStackTrace()[1].getMethodName() + "():", e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
	
	/** 抽卡 **/
	public ModelAndView lot(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			// 检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if (checkResult != null)
			{
				return checkResult;
			}
			// 获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/*********************************** 修改部分 start ************************************/
			LotJson lj = JSONObject.toJavaObject(jsonObject, LotJson.class);
			LotResultJson lrj = new LotResultJson();
			if (lj != null)
			{
				PlayerInfo pi = Cache.getInstance().getPlayerInfo(lj.playerId, request.getSession());
				if (pi != null)
				{
					if (lj.t >= 1 && lj.t <= 4)
					{
						CardGetData cgd = CardGetData.getData(lj.t);
						int packageNum = Statics.getPackageNum(pi);
						if (pi.getCards().size() + cgd.gettime > packageNum)
						{
							lrj.errorCode = 53;
							int time = (int) (24 * 3600 * 1000 - (System.currentTimeMillis() - pi.player.getLastFreeDrawTime())) / 1000;
							if (time < 0)
							{
								time = 0;
							}
							lrj.t = time;
						}
						else
						{
							boolean canNext = false;
							// 扣除相应的货币
							switch (cgd.type)
							{
								case 1:// 友情值
									if (pi.player.getFriend() >= cgd.cost)
									{
										pi.player.setFriend(pi.player.getFriend() - cgd.cost);
										canNext = true;
									}
									break;
								case 2:// 水晶							
									if(lj.t==2){
										if (pi.player.getNewPlayerType() == 1) {
											canNext = true;
											pi.player.setLastFreeDrawTime(System.currentTimeMillis());
										} else {
											if (pi.player.getLastFreeDrawTime() == 0) {
												canNext = true;
												pi.player.setLastFreeDrawTime(System.currentTimeMillis());
											} else {
												int time = (int) (24 * 3600 * 1000 - (System.currentTimeMillis() - pi.player.getLastFreeDrawTime())) / 1000;
												if (time < 0) {
													time = 0;
												}
												if (time == 0) {
													canNext = true;
													pi.player.setLastFreeDrawTime(System.currentTimeMillis());
												} else {
													if (pi.player.getTotalCrystal() >= cgd.cost) {
														int[] crystals = pi.player.removeCrystal(cgd.cost);
														MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(),cgd.name,crystals));
														canNext = true;
													}
												}
											}
										}
									}else{
										if (pi.player.getTotalCrystal() >= cgd.cost) {
											int[] crystals = pi.player.removeCrystal(cgd.cost);
											MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(),cgd.name,crystals));
											canNext = true;
										}
									}
									break;
								case 3:// 屌丝券
									if (pi.getTargetItemsNum(Constant.DiaosItemId) >= cgd.cost)
									{
										pi.removeItem(Constant.DiaosItemId, cgd.cost);
										canNext = true;
									}
									break;
							}
							// 如果转盘在活动期间，则赠送给玩家抽奖次数
							Activity a = Cache.getInstance().getActivityById(8);
							if (a.getSttime() != null && a.getEndtime() != null && a.getSttime().length() > 0 && a.getEndtime().length() > 0)
							{
								if (StringUtil.getDateTime(System.currentTimeMillis()).compareTo(a.getEndtime()) < 0 && StringUtil.getDateTime(System.currentTimeMillis()).compareTo(a.getSttime()) > 0)
								{
									pi.player.addLottoTimes(5, cgd.type);
								}
							}
							
							if (canNext)
							{
								// 已拥有的卡牌ids
								List<Integer> ownCardIds = new ArrayList<Integer>();
								for (Card c : pi.getCards())
								{
									if (!ownCardIds.contains(c.getCardId()))
									{
										ownCardIds.add(c.getCardId());
									}
								}
								// 水晶抽卡点数满10,抽一次特殊卡
								int specialCardHouse = 0;
								if (lj.t == 2)
								{
									if (pi.player.getLotPort() >= 10)
									{
										pi.player.setLotPort(0);
										specialCardHouse = 1000;
									}
									else
									{
										pi.player.addLotPort();
									}
									// 新手钻石抽卡后的首次钻石抽卡必抽5星的
									if (pi.player.getNewPlayerType() > 1 && pi.player.getFirstLot() == 0)
									{
										pi.player.setFirstLot(1);
										specialCardHouse = 1000;
										playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|过新手钻石抽卡后的首次钻石抽卡");
									}
								}
								if (lj.t == 3)
								{
									specialCardHouse = 1000;
								}
								for (int k = 0; k < cgd.gettime; k++)
								{
									if (k == 0)
									{
										lot(pi, cgd, lrj, ownCardIds, specialCardHouse);
									}
									else
									{
										lot(pi, cgd, lrj, ownCardIds, 0);
									}
								}
								// 十连抽
								if (cgd.gettime == 10)
								{
									GmSpecialMailUtil.getInstance().saveChouCardLog(pi.player.getId(), pi.player.getName(), pi.player.getLevel());
								}
								lrj.n = pi.player.getLotPort();
								lrj.d = pi.getTargetItemsNum(Constant.DiaosItemId);
								lrj.f = pi.player.getFriend();
								lrj.c = pi.player.getTotalCrystal();
								int time = (int) (24 * 3600 * 1000 - (System.currentTimeMillis() - pi.player.getLastFreeDrawTime())) / 1000;
								if (time < 0)
								{
									time = 0;
								}
								lrj.t = time;
								// 若为新手,修改数据库新手标识
								// 新手抽卡-孙悟空
								if (pi.player.getNewPlayerType() == 1)
								{
									pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
								}
								// 新手屌丝券抽卡
								if (pi.player.getNewPlayerType() == 10)
								{
									pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
								}
								// 记录每日任务进度
								ActivityController.updateTaskComplete(9, pi, 1);
								pi.player.addActive(9, 1);
							}
							else
							{
								lrj.errorCode = 19;
							}
							playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|抽卡");
						}
						// 如果转盘在活动期间，则赠送给玩家抽奖次数
						Activity a = Cache.getInstance().getActivityById(8);
						if (a.getSttime() != null && a.getEndtime() != null && a.getSttime().length() > 0 && a.getEndtime().length() > 0)
						{
							if (StringUtil.getDateTime(System.currentTimeMillis()).compareTo(a.getEndtime()) < 0 && StringUtil.getDateTime(System.currentTimeMillis()).compareTo(a.getSttime()) > 0)
							{
								pi.player.addLottoTimes(5, cgd.type);
							}
						}
					}
					if (lj.t == 11 || lj.t == 12)
					{
						long curTime = System.currentTimeMillis();
						String curDate = StringUtil.getDateTime(curTime);
						Activity activity = Cache.getInstance().getActivityById(9);
						int errorCode = 0;
						// 校验活动是否已有
						if (activity == null)
						{
							errorCode = 100;
						}
						if (errorCode == 0)
						{
							if (activity.sttime.compareTo(activity.endtime) > 0)
							{
								errorCode = 130;
							}
							if (activity.sttime.compareTo(curDate) > 0 || (activity.endtime + " 23:59:59").compareTo(curDate) < 0)
							{
								errorCode = 114;
							}
						}
						// 检验背包
						if (errorCode == 0)
						{
							int packageNum = Statics.getPackageNum(pi);
							if (pi.getCards().size() + 1 > packageNum)
							{
								errorCode = 53;
							}
						}
						// 校验是否可以抽
						if (errorCode == 0)
						{
							TakeCardTimeData tctd = TakeCardTimeData.getData(1);
							long time = tctd.time * 3600 * 1000;
							LotRank lr = Cache.getInstance().getLotRankById(pi.player.getId());
							if (lr != null)
							{
								if (lj.t == 11)
								{
									if (curTime - (lr.getLastLotTime() + time) < 0)
									{
										errorCode = 127;
									}
								}
								if (lj.t == 12)
								{
									if (lr.getScoreLotNum()<=0)
									{
										if (lr.getScore() % tctd.interracialcast > 0)
										{
											if (pi.player.getCrystal() + pi.player.getCrystalPay() < tctd.cast)
											{
												errorCode = 71;
											}
										}
									}
								}
							}
						}
						// 功能那阶段
						if (errorCode == 0)
						{
							// 已拥有的卡牌ids
							List<Integer> ownCardIds = new ArrayList<Integer>();
							for (Card c : pi.getCards())
							{
								if (!ownCardIds.contains(c.getCardId()))
								{
									ownCardIds.add(c.getCardId());
								}
							}
							LotRank lr = Cache.getInstance().getLotRankById(pi.player.getId());
							TakeCardTimeData tctd = TakeCardTimeData.getData(1);
							if (lr != null)
							{
								if (lj.t == 12)
								{
									if (lr.getScoreLotNum() > 0)
									{
										lr.setScoreLotNum(lr.getScoreLotNum() - 1);
										Cache.getInstance().addLotRanks(lr);
									}
									else
									{
										if ((lr.getScore() % tctd.interracialcast) > 0)
										{
											pi.player.removeCrystal(tctd.cast);
										}
									}
								}
							}
							else
							{
								if (lj.t == 12)
								{
									pi.player.removeCrystal(tctd.cast);
								}
							}
							// lotRank(curTime, pi, tctd);
							LotRank l = null;
							int cardId = CardkuData.getCardIdByKuType(lj.cs);
							if (cardId != 0)
							{
								l = new LotRank();
								if (lr != null)
								{
									if (lr.getScore() % tctd.interracialcast == 0 && lj.t == 11)
									{
										l.setScoreLotNum(lr.getScoreLotNum() + 1);
									}
									else
									{
										l.setScoreLotNum(lr.getScoreLotNum());
									}
									
									l.setId(lr.getId());
									l.setLotNum(lr.getLotNum() + 1);
									l.setScore(lr.getScore() + tctd.getintegral);
									l.setRank(lr.getRank());
									if (lj.t == 11)
									{
										l.setLastLotTime(curTime);
									}
									if (lj.t == 12)
									{
										l.setLastLotTime(lr.getLastLotTime());
									}
								}
								else
								{
									l.setScoreLotNum(0);
									l.setScore(tctd.getintegral);
									l.setLotNum(1);
									l.setRank(10000);
									if (lj.t == 11)
									{
										l.setLastLotTime(curTime);
									}
									if (lj.t == 12)
									{
										l.setLastLotTime(0);
									}
								}
								
								l.setPlayerId(pi.player.getId());
								Cache.getInstance().addLotRanks(l);
								Card c = pi.addCard(cardId, 1);
								List<Integer> cardIds = new ArrayList<Integer>();
								cardIds.add(cardId);
								if (cardIds.size() > 0)
								{
									pi.getNewUnitSkill(cardIds);
								}
								if (lrj.list == null)
								{
									lrj.list = new ArrayList<PackElement>();
								}
								if (c != null)
								{
									int index = pi.getCards().indexOf(c);
									PackElement pe = Statics.createPackElement(index, c, pi);
									pe.nw = ownCardIds.contains(pe.dataId) ? 0 : 1;
									lrj.list.add(pe);
								}
							}
							else
							{
								logger.info("没有抽到卡");
							}
						}
						else
						{
							lrj.errorCode = errorCode;
						}
					}
				}
				else
				{
					lrj.errorCode = -3;
				}
			}
			else
			{
				lrj.errorCode = 18;
			}
			Cache.recordRequestNum(lrj);
			String msg = JSON.toJSONString(lrj);
			/*********************************** 修改部分 end **************************************/
			logger.info("msg:" + msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e)
		{
			errorlogger.error(this.getClass().getName() + "->" + Thread.currentThread().getStackTrace()[1].getMethodName() + "():", e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
	
	private static void lot(PlayerInfo pi, CardGetData cgd, LotResultJson lrj, List<Integer> ownCardIds, int specialCardHouse)
	{
		// 目标卡库
		int cardHouse = 0;
		if (specialCardHouse == 0)
		{
			// 已有的幸运值
			int oldLuck = pi.player.getLuckyNum();
			// 抽卡累加幸运值
			int luck = cgd.luckyadd;
			// 幸运值=抽卡累加幸运值+额外幸运值+已有的幸运值
			int newLuck = luck + Constant.extendLuckNum + oldLuck;
			// 设置最新抽卡幸运值
			pi.player.setLuckyNum(newLuck);
			// 根据抽卡幸运值选取一组
			for (int i = 0; i < cgd.luckys.size(); i++)
			{
				int tempLuck = cgd.luckys.get(i);
				if (oldLuck < tempLuck && newLuck >= tempLuck)
				{
					cardHouse = cgd.cardHouses.get(i);
					if (i == 0)
					{
						pi.player.setLuckyNum(newLuck - tempLuck);
					}
					break;
				}
			}
			if (cardHouse == 0)
			{
				for (int i = 0; i < cgd.luckys.size(); i++)
				{
					int tempLuck = cgd.luckys.get(i);
					if (tempLuck != 0)
					{
						int mul = newLuck / tempLuck;
						if (oldLuck < mul * tempLuck && newLuck >= mul * tempLuck)
						{
							cardHouse = cgd.cardHouses.get(i);
							break;
						}
					}
				}
			}
			if (cardHouse == 0)
			{
				cardHouse = cgd.cardHouses.get(cgd.cardHouses.size() - 1);
			}
		}
		else
		{
			cardHouse = specialCardHouse;
		}
		if (cardHouse != 0)
		{
			int cardId = 0;
			if (pi.player.getNewPlayerType() == 1)// 新手初次抽卡
			{
				cardId = 14001;// 四星孙悟空
			}
			else if (pi.player.getNewPlayerType() == 10)// 新手初次号角抽卡必得杨戬
			{
				cardId = 14002;// 杨戬
			}
			else
			{
				cardId = CardBoxData.getCardIdByHouseType(cardHouse);
			}
			Card c = pi.addCard(cardId, 1);
			List<Integer> cardIds = new ArrayList<Integer>();
			cardIds.add(cardId);
			if (cardIds.size() > 0)
			{
				pi.getNewUnitSkill(cardIds);
			}
			CardData cData = CardData.getData(cardId);
			if (cData != null)
			{
				// --抽到5星级以上卡发公告--//
				if (cData.star >= 5)
				{
					Announce e = Announce.createAnnounce(0, "恭喜玩家" + pi.player.getName() + "获得" + cData.star + "星卡牌" + cData.name + "！", 1, 1, 1);
					Cache.instance.addAnnounce(e);
				}
				else if (cData.star == 4)
				{
					Announce e = Announce.createAnnounce2(0, "恭喜玩家" + pi.player.getName() + "获得" + cData.star + "星卡牌" + cData.name + "！", 1, 1, 1);
					Cache.instance.addAnnounce(e);
				}
			}
			if (lrj.list == null)
			{
				lrj.list = new ArrayList<PackElement>();
			}
			if (c != null)
			{
				int index = pi.getCards().indexOf(c);
				PackElement pe = Statics.createPackElement(index, c, pi);
				pe.nw = ownCardIds.contains(pe.dataId) ? 0 : 1;
				lrj.list.add(pe);
			}
		}
	}
	
	/** 迷宫战前初始化数据 **/
	public ModelAndView mazeBattleInit(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			// 检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if (checkResult != null)
			{
				return checkResult;
			}
			// 获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			// 获取参数
			MazeBattleJson mbj = JSONObject.toJavaObject(jsonObject, MazeBattleJson.class);
			MazeBattleResultJson mbrj = new MazeBattleResultJson();
			if (mbj != null)
			{
				PlayerInfo pi = Cache.getInstance().getPlayerInfo(mbj.playerId, request.getSession());
				int errorcode = 0;
				if (pi != null)
				{
					// 校验阶段
					MazeData mData = MazeData.getMazeData(mbj.td);
					CardGroup cg = pi.getCardGroup();
					if (mData != null)
					{
						if (mData.condition0 == 1)// 等级解锁
						{
							if (mData.condition > pi.player.getLevel())
							{
								errorcode = 25;
							}
						}
						else if (mData.condition0 == 2)// 关卡解锁
						{
							if (mData.condition > pi.player.getMissionId())
							{
								errorcode = 25;
							}
						}
					}
					else
					{
						errorcode = 26;
					}
					if (errorcode == 0)// 校验卡组是否有卡
					{
						if (!cg.canFight())
						{
							errorcode = 65;
						}
					}
					// 功能阶段
					if (errorcode == 0)
					{
						// 随机战斗data
						MazeBattleData mbData = MazeBattleData.getMazeBattleDataRandom(mbj.td, mbj.t);
						// 判断是否有许愿碎片
						String mazeWish = pi.player.getMazeWish();
						int itemID = 0;
						if (mbData.type == 3)
						{
							if (mazeWish != null && mazeWish.length() > 0)
							{
								String[] mazeDrop = mazeWish.split("&");
								for (int j = 0; j < mazeDrop.length; j++)
								{
									String[] temp = mazeDrop[j].split("-");
									if (StringUtil.getInt(temp[0]) == mbj.td)
									{
										itemID = StringUtil.getInt(temp[1]);
										break;
									}
								}
							}
						}
						String maze = pi.player.getMaze();
						int mazeNum = 0;// m迷宫进入次数
						
						if (maze != null && maze.length() > 0)
						{
							String[] mazeStr = maze.split(",");
							for (int k = 0; k < mazeStr.length; k++)
							{
								if (mazeStr[k] != null && mazeStr[k].length() > 0)
								{
									String[] mazes = mazeStr[k].split("-");
									if (mazes[0] != null && mazes[0].length() > 0)
									{
										if (Integer.parseInt(mazes[0]) == mbj.td)
										{
											if (mazes[2] != null && mazes[2].length() > 0)
											{
												mazeNum = Integer.parseInt(mazes[2]);
												break;
											}
										}
									}
								}
								else
								{
									continue;
								}
							}
						}
						// 第一次boss战(第一次进入迷宫)
						if (mbj.getT() == 2 && mazeNum == 0)
						{
							String fristentry = pi.player.getMazeFirstEntry();
							boolean first = false;
							if (fristentry == null || fristentry.length() == 0)
							{
								first = true;
							}
							else
							{
								String[] fe = fristentry.split("&");
								HashMap<Integer, String> map = new HashMap<Integer, String>();
								for (int m = 0; m < fe.length; m++)
								{
									if (fe[m] != null && fe[m].length() > 0)
									{
										map.put(StringUtil.getInt(fe[m]), fe[m]);
									}
								}
								if (!map.containsKey(mbj.td))
								{
									first = true;
								}
							}
							if (first)
							{
								MazeProbabilityData mpd = MazeProbabilityData.getMpData(mbj.td, 2);
								int dropId = mpd.awardfirst;
								List<MazeSkillDropData> list = new ArrayList<MazeSkillDropData>();
								list = MazeSkillDropData.getRandomMazeSkillDropData(dropId);
								int randnum = Random.getNumber(0, 1000);
								int st = 0;
								int end = 0;
								String[] s = new String[1];
								for (int i = 0; i < list.size(); i++)
								{
									end = list.get(i).probability + st;
									if (randnum > st && randnum <= end)
									{
										s[0] = list.get(i).type + "-" + list.get(i).skillID;
										break;
									}
									st = end;
								}
								// 掉落物品
								mbrj.s = s;
							}
							else
							{
								mbrj.s = Statics.mazeDropThings(mbData, cg, itemID);
							}
						}
						// 普通战,非第一次boss战
						else
						{
							// 生成掉落物品
							mbrj.s = Statics.mazeDropThings(mbData, cg, itemID);
						}
						
						mbrj.td = mbj.td;
						mbrj.md = mbData.id;
						mbrj.aF = 0;
						if (pi.mazes!=null && pi.mazes.size()>0)
						{
							mbrj.cs0 = pi.getBattleData(mbj.td);
						}
						if (mbrj.cs0 == null || mbrj.cs0.length<=0)
						{
							mbrj.cs0 = cg.getBattleData();
						}
						mbrj.cs1 = MazeBattleData.getMonstersData(mbData.id);
						// 双方合体技
						int[] us0 = new int[3];
						us0[0] = cg.unitId;
						mbrj.us0 = us0;
						mbrj.bNum = pi.bNum + 1;
						mbrj.type = mbj.t;
						mbrj.state = mbj.state;
						int[] maxEnergys = { pi.player.getMaxEnergy(), 0 };
						mbrj.mes = maxEnergys;
						mbrj.initE = VipData.getInitEnergy(pi.player.getVipLevel());
						mbrj.raceAtts = Statics.getRaceAttris(pi);
						pi.mbrj = mbrj;
						
					}
					mbrj.errorCode = errorcode;
					playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|进入迷宫战斗");
				}
				else
				{
					mbrj.errorCode = -3;
				}
			}
			else
			{
				mbrj.errorCode = -1;
			}
			Cache.recordRequestNum(mbrj);
			String msg = JSON.toJSONString(mbrj);
			logger.info("msg:" + msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e)
		{
			errorlogger.error(this.getClass().getName() + "->" + Thread.currentThread().getStackTrace()[1].getMethodName() + "():", e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
	
	/** 领取全3星奖励 **/
	public ModelAndView getStar3Reward(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			// 检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if (checkResult != null)
			{
				return checkResult;
			}
			// 获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/*********************************** 修改部分 start ************************************/
			Star3RewardJson s3rj = JSONObject.toJavaObject(jsonObject, Star3RewardJson.class);
			MapResultJson ej = new MapResultJson();
			PlayerInfo pi = Cache.getInstance().getPlayerInfo(s3rj.playerId, request.getSession());
			if (pi != null)
			{
				String[] ss = s3rj.m.split("-");
				int map = StringUtil.getInt(ss[0]);
				int zone = StringUtil.getInt(ss[1]);
				int missionType = StringUtil.getInt(ss[2]);
				int[] sequences = MissionData.getSequences(map, zone, missionType);
				// 校验是否可以领3星奖励
				String stars = "";
				if (missionType == 1)
				{
					stars = pi.player.getCompleteStar1();
				}
				else
				{
					stars = pi.player.getCompleteStar2();
				}
				if (stars.length() < sequences[1])
				{
					ej.errorCode = 63;
				}
				if (ej.errorCode == 0)
				{
					String subStars = stars.substring(sequences[0] - 1, sequences[1]);
					for (int i = 0; i < subStars.length(); i++)
					{
						char c = subStars.charAt(i);
						if (c == '1' || c == '2')
						{
							ej.errorCode = 63;
							break;
						}
						if (c == '4')
						{
							ej.errorCode = 64;
							break;
						}
					}
				}
				if (ej.errorCode == 0)
				{
					stars = stars.substring(0, sequences[1] - 1) + "4" + stars.substring(sequences[1], stars.length());
					AllThreeStarData starData = AllThreeStarData.getData(s3rj.m);
					List<Integer> cardIds = new ArrayList<Integer>();
					if (starData.rewardtype == 3)
					{
						String[] temp = starData.reward.split(",");
						cardIds.add(StringUtil.getInt(temp[0]));
					}
					Statics.getReward(starData.rewardtype, starData.reward, pi);
					if (cardIds.size() > 0)
					{
						pi.getNewUnitSkill(cardIds);
					}
					if (missionType == 1)
					{
						pi.player.setCompleteStar1(stars);
					}
					else
					{
						pi.player.setCompleteStar2(stars);
					}
					ej.m1 = pi.player.getMissionId();
					ej.m2 = pi.player.getMissionId2();
					ej.star1 = pi.player.getCompleteStar1();
					ej.star2 = pi.player.getCompleteStar2();
					ej.b1 = pi.player.getBouns1();
					ej.b2 = pi.player.getBouns2();
					ej.t1 = pi.player.getTimes1();
					ej.t2 = pi.player.getTimes2();
					ej.c = pi.player.getTotalCrystal();
					ej.d = pi.player.getGold();
					ej.kopoint = pi.player.getKopoint();
					playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|领取全三星奖励," + s3rj.m);
				}
			}
			else
			{
				ej.errorCode = -3;
			}
			Cache.recordRequestNum(ej);
			String msg = JSON.toJSONString(ej);
			/*********************************** 修改部分 end **************************************/
			logger.info("msg:" + msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e)
		{
			errorlogger.error(this.getClass().getName() + "->" + Thread.currentThread().getStackTrace()[1].getMethodName() + "():", e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
	
	/** 卡牌突破 **/
	public ModelAndView cardBreak(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			ModelAndView checkResult = RequestUtil.check(request);
			if (checkResult != null)
			{
				return checkResult;
			}
			// 获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			// 获取参数
			BreakJson bj = JSONObject.toJavaObject(jsonObject, BreakJson.class);
			BreakResultJson brj = new BreakResultJson();
			if (bj != null)
			{
				PlayerInfo pi = Cache.getInstance().getPlayerInfo(bj.playerId, request.getSession());
				if (pi != null)
				{
					List<Card> cards = pi.getCards();
					// 玩家拥有此卡牌
					int pCardN = 0;
					// 玩家拥有万能突破卡牌数量
					int multCard = 0;
					// 校验阶段
					int errorcode = 0;
					if (bj.index < 0 || bj.index > cards.size())
					{
						errorcode = 6;
					}
					else
					{
						Card c = cards.get(bj.index);
						CardData cardData = CardData.getData(c.getCardId());
						if (c.getBreakNum() == 7)// 校验目标卡是否突破上限
						{
							errorcode = 67;
						}
						if (errorcode == 0)// 校验目标卡是否可以突破(神格卡，经验卡，金币卡,突破次数为5的卡)
						{
							if (cardData.race == 6 || cardData.race == 7 || cardData.element == 5)
							{
								errorcode = 68;
							}
						}
						if (errorcode == 0)// 校验被吃的卡
						{
							List<PackElement> pes = new ArrayList<PackElement>();
							pes = UiController.getElements(pi, 1);
							for (int i = pes.size() - 1; i >= 0; i--)
							{
								if (pes.get(i) != null)
								{
									CardData cData = CardData.getData(pes.get(i).getDataId());
									if (pes.get(i).getUse() == 0)
									{// 不在卡组中的卡
										if (pes.get(i).i != bj.index && cData != null && cData.race == 6 && cData.star == cardData.star)// 同星级神格卡
										{
											pCardN++;
										}
										else if (pes.get(i).i != bj.index && cData != null && cData.id == cardData.id)// 同卡
										{
											multCard++;
										}
									}
								}
							}
							// 突破所需要的卡
							EvolutionData evD = EvolutionData.getData(cardData.star, c.getBreakNum() + 1);
							int num = pCardN + multCard;
							if (evD.cards > num)
							{
								errorcode = 129;
							}
						}
						if (errorcode == 0)// 校验金罡心
						{
							if (pi.player.getNewPlayerType() != 20)
							{
								CardData cData = CardData.getData(c.getCardId());
								EvolutionData evoData = EvolutionData.getData(cData.star, c.getBreakNum() + 1);
								if (evoData.moneypercard > pi.player.getDiamond())
								{
									errorcode = 126;
								}
							}
						}
					}
					// if (errorcode == 0)
					// {
					// if (pi.player.getNewPlayerType() == 20)
					// {
					// Card card = pi.getCards().get(bj.index);
					// if (card.getCardId() != 14001)
					// {
					// errorcode = 74;// 突破卡不是孙悟空
					// }
					// }
					// }
					// 功能阶段
					if (errorcode == 0)
					{
						Card c = pi.getCards().get(bj.index);
						CardData cardData = CardData.getData(c.getCardId());
						EvolutionData evoData = EvolutionData.getData(cardData.star, c.getBreakNum() + 1);
						int cardNum = evoData.cards;
						int n = 0;
						if (bj.list != null && bj.list.size() > 0)
						{
							for (int k = 0; k < bj.list.size(); k++)
							{
								Card card = cards.get(bj.list.get(k));
								if (n < cardNum)
								{
									card.setSell(2);
								}
								else
								{
									break;
								}
								n++;
								playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|突破消耗card：" + card.getCardId());
							}
						}
						c.setBreakNum(c.getBreakNum() + 1);
						c.setBreakType(c.getBreakType() + 1);
						pi.player.removeDiamond(evoData.moneypercard);
						// 玩家拥有此卡牌
						int pCardN2 = 0;
						// 玩家拥有万能突破卡牌数量
						int multCard2 = 0;
						List<PackElement> pes = new ArrayList<PackElement>();
						List<PackElement> list = new ArrayList<PackElement>();
						pes = UiController.getElements(pi, 1);
						for (int i = pes.size() - 1; i >= 0; i--)
						{
							if (pes.get(i) != null)
							{
								CardData cData = CardData.getData(pes.get(i).getDataId());
								if (pes.get(i).getUse() == 0)
								{// 不在卡组中的卡
									if (pes.get(i).i != bj.index && cData != null && cData.race == 6 && cData.star == cardData.star)// 同星级神格卡
									{
										multCard2++;
										list.add(pes.get(i));
									}
									else if (pes.get(i).i != bj.index && cData != null && cData.id == cardData.id)// 同卡
									{
										pCardN2++;
										list.add(pes.get(i));
									}
								}
							}
							
						}
						brj.pes = list;
						if (c.getBreakNum() < 7)
						{
							EvolutionData evoData2 = EvolutionData.getData(cardData.star, c.getBreakNum() + 1);
							brj.cn = evoData2.cards;
							brj.d = evoData2.moneypercard;
						}
						else
						{
							brj.cn = -1;
							brj.d = -1;
						}
						brj.pcn = pCardN2;
						brj.pd = pi.player.getDiamond();
						brj.pmc = multCard2;
						brj.pe = Statics.createPackElement(pi.getCards().indexOf(c), c, pi);
						if (pi.player.getNewPlayerType() == 20)// 新手突破
						{
							pi.player.addDiamond(evoData.moneypercard);
							pi.player.setNewPlayerType(pi.player.getNewPlayerType() + 1);
							brj.sell = 1;
							
						}
						else
						{
							brj.sell = 0;
						}
						// 更新成就
						List<Card> cardList = pi.getCards();
						List<AchievementData> ads = AchievementData.getAchievementByType(11);
						for (int j = 0; j < ads.size(); j++)
						{
							AchievementData ad = ads.get(j);
							String req = ad.request;
							String[] result = req.split(",");
							int num = 0;
							int bnum = StringUtil.getInt(result[1]);
							for (int i = 0; i < cardList.size(); i++)
							{
								if (cardList.get(i).getBreakNum() == bnum)
								{
									num++;
								}
							}
							pi.player.updateAchieve(11, num + "," + bnum);
						}
						playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|卡牌突破");
						// 如果转盘在活动期间。则根据突破赠送玩家抽奖次数
						Activity a = Cache.getInstance().getActivityById(8);
						if (a.getSttime() != null && a.getEndtime() != null && a.getSttime().length() > 0 && a.getEndtime().length() > 0)
						{
							if (StringUtil.getDateTime(System.currentTimeMillis()).compareTo(a.getEndtime()) < 0 && StringUtil.getDateTime(System.currentTimeMillis()).compareTo(a.getSttime()) > 0)
							{
								pi.player.addLottoTimes(2, c.getBreakNum());
							}
						}
					}
					else
					{
						brj.errorCode = errorcode;
					}
				}
				else
				{
					brj.errorCode = -3;
				}
			}
			else
			{
				brj.errorCode = -1;
			}
			Cache.recordRequestNum(brj);
			String msg = JSON.toJSONString(brj);
			logger.info("msg:" + msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e)
		{
			errorlogger.error(this.getClass().getName() + "->" + Thread.currentThread().getStackTrace()[1].getMethodName() + "():", e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
	
	/** 计算突破及总消耗金币 **/
	public static String addBreakNum(Card c, List<Integer> list, PlayerInfo pi)
	{
		List<Card> cards = pi.getCards();
		int num = 0;
		// 计算总卡数
		for (int i : list)
		{
			Card card = cards.get(i);
			CardData cData = CardData.getData(card.getCardId());
			if (card.getBreakNum() > 0)
			{
				EvolutionData eData = EvolutionData.getData(cData.star, card.getBreakNum());
				num = num + eData.cards + card.getBreakType();
			}
			else
			{
				num = num + 1;
			}
		}
		// 计算金 罡心
		CardData cd = CardData.getData(c.getCardId());
		int gold = 0;
		int breaknum = c.getBreakNum();
		int breakType = c.getBreakType();
		if (cd == null)
		{
			errorlogger.info("目标卡为null");
		}
		while (num > 0)
		{
			EvolutionData evolutionData = EvolutionData.getData(cd.star, breaknum + 1);
			if (evolutionData != null)
			{
				if (num + c.getBreakType() >= evolutionData.cards)
				{
					num = num + c.getBreakType() - evolutionData.cards;
					gold = gold + (evolutionData.cards - c.getBreakType()) * evolutionData.moneypercard;
					if (breaknum < 5)
					{
						breaknum++;
					}
					else
					{
						breakType = 0;
						break;
					}
					breakType = 0;
					
				}
				else
				{
					breakType = breakType + num;
					gold = gold + evolutionData.moneypercard * num;
					num = 0;
				}
			}
			else
			{
				num = 0;
			}
		}
		return breaknum + "-" + gold + "-" + breakType;
	}
	
	/** 扫荡界面 **/
	public ModelAndView sweepUi(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			ModelAndView checkResult = RequestUtil.check(request);
			if (checkResult != null)
			{
				return checkResult;
			}
			// 获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			// 获取参数
			SweepUiJson suj = JSONObject.toJavaObject(jsonObject, SweepUiJson.class);
			SweepUiResultJson surj = new SweepUiResultJson();
			if (suj != null)
			{
				PlayerInfo pi = Cache.getInstance().getPlayerInfo(suj.playerId, request.getSession());
				if (pi != null)
				{
					int md = suj.md;
					MissionData mData = MissionData.getMissionData(md);
					List<Item> items = pi.getTargetItems(80001);
					int sequence = 0;
					int num = 0;// 关卡已经挑战次数
					if (mData != null)
					{
						sequence = mData.getSequence();
						int star = 0;
						// 判断挑战次数 和星级
						if (mData.missiontype == 1)
						{
							String times = pi.player.getTimes1();
							int beginIndex = (sequence - 1) * 2;
							int endIndex = beginIndex + 2;
							num = StringUtil.getInt(times.substring(beginIndex, endIndex));
							String starLevel = pi.player.getCompleteStar1();
							star = StringUtil.getInt(StringUtil.getStr(starLevel, sequence - 1));
						}
						else
						{
							String times = pi.player.getTimes2();
							int beginIndex = (sequence - 1) * 2;
							int endIndex = beginIndex + 2;
							num = StringUtil.getInt(times.substring(beginIndex, endIndex));
							String starLevel = pi.player.getCompleteStar2();
							star = StringUtil.getInt(StringUtil.getStr(starLevel, sequence - 1));
						}
						if (star < 3)
						{
							surj.errorCode = 88;
						}
					}
					int sweepNum = mData.times - num;
					if (pi.bNum == 0)
					{
						surj.bNum = 0;
					}
					else
					{
						surj.bNum = pi.bNum;
					}
					surj.entryTimes = mData.times - num;
					surj.power = mData.cost;
					int sweepTimes = 0;
					if (items != null && items.size() > 0)
					{
						surj.itemNum = pi.getTargetItemsNum(80001);
					}
					surj.md = md;
					if (sweepNum < 10)// 副本剩余挑战次数<10
					{
						if (sweepNum > 0)
						{
							if (pi.player.getPower() >= sweepNum * mData.cost)
							{
								sweepTimes = sweepNum;
							}
							else
							{
								sweepTimes = pi.player.getPower() / mData.cost;
							}
						}
						else if (num == 0)
						{
							surj.errorCode = 50;
						}
						
					}
					else
					{
						if (pi.player.getPower() >= 10 * mData.cost)
						{
							sweepTimes = 10;
						}
						else
						{
							sweepTimes = pi.player.getPower() / mData.cost;
						}
					}
					if (sweepTimes < 1)
					{
						sweepTimes = 0;
					}
					surj.sweepTimes = sweepTimes;
					playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|请求扫荡界面");
				}
				else
				{
					surj.errorCode = -3;
				}
			}
			else
			{
				surj.errorCode = -1;
			}
			
			String msg = JSON.toJSONString(surj);
			logger.info("msg:" + msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e)
		{
			errorlogger.error(this.getClass().getName() + "->" + Thread.currentThread().getStackTrace()[1].getMethodName() + "():", e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
	
	/** 扫荡 **/
	public ModelAndView sweep(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			ModelAndView checkResult = RequestUtil.check(request);
			if (checkResult != null)
			{
				return checkResult;
			}
			// 获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			// 获取参数
			SweepJson sj = JSONObject.toJavaObject(jsonObject, SweepJson.class);
			SweepResultJson srj = new SweepResultJson();
			if (sj != null)
			{
				PlayerInfo pi = Cache.getInstance().getPlayerInfo(sj.playerId, request.getSession());
				if (pi != null)
				{
					int md = sj.md;
					int sweepNum = sj.sweepNum;
					MissionData mData = MissionData.getMissionData(md);
					// 校验阶段
					int errorcode = 0;
					/** 校验vip等级 **/
					if (errorcode == 0)
					{
						VipData vData = VipData.getVipData(pi.player.getVipLevel());
						if (vData != null)
						{
							if (sweepNum == 1)
							{
								if (vData.mission == 0)
								{
									errorcode = 70;
								}
							}
							else if (sweepNum > 1)
							{
								if (vData.missiontimes == 0)
								{
									errorcode = 70;
								}
							}
							
						}
					}
					
					// 判断星级
					int star = 0;
					int sequence = mData.getSequence();
					// 判断挑战次数
					int num = 0;
					if (mData.missiontype == 1)
					{
						String times = pi.player.getTimes1();
						int beginIndex = (sequence - 1) * 2;
						int endIndex = beginIndex + 2;
						num = StringUtil.getInt(times.substring(beginIndex, endIndex));
						String starLevel = pi.player.getCompleteStar1();
						star = StringUtil.getInt(StringUtil.getStr(starLevel, sequence - 1));
					}
					else
					{
						String times = pi.player.getTimes2();
						int beginIndex = (sequence - 1) * 2;
						int endIndex = beginIndex + 2;
						num = StringUtil.getInt(times.substring(beginIndex, endIndex));
						String starLevel = pi.player.getCompleteStar2();
						star = StringUtil.getInt(StringUtil.getStr(starLevel, sequence - 1));
					}
					if (star < 3)// 校验星级
					{
						errorcode = 88;
					}
					// 体力
					if (errorcode == 0)
					{
						if (pi.player.getPower() < sweepNum * mData.cost)
						{
							errorcode = 27;
						}
					}
					
					if (errorcode == 0)// 校验挑战次数
					{
						if (num >= mData.times)
						{
							errorcode = 50;
						}
					}
					// 校验扫荡券80001
					if (errorcode == 0)
					{
						int pile = pi.getTargetItemsNum(80001);
						if (pile < sweepNum)
						{
							errorcode = 83;
						}
					}
					if (errorcode == 0)
					{
						int packageNum = Statics.getPackageNum(pi);
						if (pi.getCards().size() >= packageNum || pi.getSkills().size() >= packageNum || pi.getPassiveSkillls().size() >= packageNum || pi.getEquips().size() >= packageNum || pi.getItems().size() >= packageNum)
						{
							errorcode = 53;
							BagCostData bd = BagCostData.getData(pi.player.getBuyBagTimes() + 1);
							if (bd == null)
							{
								errorcode = 131;
							}
						}
					}
					// 功能阶段
					if (errorcode == 0)
					{
						
						if (mData.missiontype == 1)
						{
							ActivityController.updateTaskComplete(1, pi, sweepNum);
							pi.player.addActive(1, sweepNum);
						}
						else if (mData.missiontype == 2)
						{
							ActivityController.updateTaskComplete(1, pi, sweepNum);
							ActivityController.updateTaskComplete(2, pi, sweepNum);
							pi.player.addActive(1, sweepNum);
							pi.player.addActive(2, sweepNum);
						}
						
						List<String> ds = null;
						SweepCardJson scj = null;
						List<SweepCardJson> sweepinfo = new ArrayList<SweepCardJson>();
						CardGroup cg = pi.getCardGroup();
						List<Integer> cardIds = new ArrayList<Integer>();
						for (int i = 0; i < sweepNum; i++)
						{
							String[] drops = Statics.dropsThings(md, cg, pi);
							ds = new ArrayList<String>();
							scj = new SweepCardJson();
							for (int j = 0; j < drops.length; j++)
							{
								String drop = drops[j];
								if (drop == null || "".equals(drop))
								{
									continue;
								}
								ds.add(drop);
								String[] ss = drop.split("-");
								int type = StringUtil.getInt(ss[0]);
								
								switch (type)
								{
									case 1:// item
										String[] temp = ss[1].split(",");
										pi.addItem(StringUtil.getInt(temp[0]), StringUtil.getInt(temp[1]));
										break;
									case 2:// equip
										pi.addEquip(StringUtil.getInt(ss[1]));
										break;
									case 3:// card
										temp = ss[1].split(",");
										cardIds.add(StringUtil.getInt(temp[0]));
										pi.addCard(StringUtil.getInt(temp[0]), StringUtil.getInt(temp[1]));
										break;
									case 4:// skill
										pi.addSkill(StringUtil.getInt(ss[1]), 1);
										break;
									case 5:// passiveSkill
										pi.addPassiveSkill(StringUtil.getInt(ss[1]));
										break;
								}
							}
							if (cardIds.size() > 0)
							{
								pi.getNewUnitSkill(cardIds);
							}
							scj.playerExp = (int) (mData.personexp * Cache.getExpMul());
							scj.cardExp = mData.cardexp;
							scj.ds = ds;
							sweepinfo.add(scj);
						}
						
						// 战前卡组
						List<String> cards0 = new ArrayList<String>();
						for (Card c : cg.cards)
						{
							if (c != null)
							{
								CardData cd = CardData.getData(c.getCardId());
								int maxExp = 0;
								CardExpData ced = CardExpData.getData(c.getLevel() + 1);
								if (cd != null && ced != null)
								{
									maxExp = ced.starexps[cd.star - 1];
								}
								int maxHp = (int) Statics.getCardSelfMaxHp(c.getCardId(), c.getLevel(), c.getBreakNum());
								int atk = (int) Statics.getCardSelfMaxAtk(cd.id, c.getLevel(), c.getBreakNum());
								int def = (int) Statics.getCardSelfMaxDef(cd.id, c.getLevel(), c.getBreakNum());
								cards0.add(c.getCardId() + "-" + c.getLevel() + "-" + c.getCurExp() + "-" + maxExp + "-" + maxHp + "-" + atk + "-" + def);
							}
						}
						srj.cs0 = cards0;
						srj.ce0 = pi.player.getCurExp();
						srj.lv0 = pi.player.getLevel();
						
						pi.player.removePower(sweepNum * mData.cost);
						srj.power0 = pi.player.getPower();
						/** 每次扫荡的掉落物品等 **/
						for (int i = 0; i < sweepNum; i++)
						{
							/** 扫荡一轮之后，任务，卡牌获得经验以及金币 **/
							pi.player.addExp(mData.personexp);
							pi.player.addGold((int) Cache.getGoldDropMul() * mData.coins);
							for (Card c : cg.cards)
							{
								if (c != null)
								{
									c.addExp(mData.cardexp);
								}
							}
						}
						// 战后卡组
						List<String> cards1 = new ArrayList<String>();
						for (Card c : cg.cards)
						{
							if (c != null)
							{
								CardData cd = CardData.getData(c.getCardId());
								int maxExp = 0;
								CardExpData ced = CardExpData.getData(c.getLevel() + 1);
								ced = CardExpData.getData(c.getLevel() + 1);
								if (cd != null && ced != null)
								{
									maxExp = ced.starexps[cd.star - 1];
								}
								int maxHp = (int) Statics.getCardSelfMaxHp(c.getCardId(), c.getLevel(), c.getBreakNum());
								int atk = (int) Statics.getCardSelfMaxAtk(cd.id, c.getLevel(), c.getBreakNum());
								int def = (int) Statics.getCardSelfMaxDef(cd.id, c.getLevel(), c.getBreakNum());
								cards1.add(c.getCardId() + "-" + c.getLevel() + "-" + c.getCurExp() + "-" + maxExp + "-" + maxHp + "-" + atk + "-" + def);
							}
						}
						srj.cs1 = cards1;
						srj.ce1 = pi.player.getCurExp();
						srj.lv1 = pi.player.getLevel();
						srj.power1 = pi.player.getPower();
						// 记录挑战次数,2位字符来表示
						int sweepTimes = 0;
						int sequence1 = mData.getSequence();
						if (mData != null)
						{
							if (mData.missiontype == 1)
							{
								String times = pi.player.getTimes1();
								int beginIndex = (sequence1 - 1) * 2;
								int endIndex = beginIndex + 2;
								String s = (num + sweepNum) + "";
								if (s.length() == 1)
								{
									s = "0" + s;
								}
								times = times.substring(0, beginIndex) + s + times.substring(endIndex, times.length());
								pi.player.setTimes1(times);
								sweepTimes = mData.times - sweepNum - num;
							}
							else
							{
								String times = pi.player.getTimes2();
								int beginIndex = (sequence1 - 1) * 2;
								int endIndex = beginIndex + 2;
								String s = (num + sweepNum) + "";
								if (s.length() == 1)
								{
									s = "0" + s;
								}
								times = times.substring(0, beginIndex) + s + times.substring(endIndex, times.length());
								pi.player.setTimes2(times);
								sweepTimes = mData.times - sweepNum - num;
							}
						}
						// 扣除扫荡券个数
						pi.removeItem(80001, sweepNum);
						srj.sweepInfo = sweepinfo;
						srj.power = mData.cost;
						srj.entryTimes = sweepTimes;
						List<Item> its = pi.getTargetItems(80001);
						if (its != null && its.size() > 0)
						{
							srj.itemNum = pi.getTargetItemsNum(80001);
						}
						else
						{
							srj.itemNum = 0;
						}
						if (sweepTimes >= 10)
						{
							if (pi.player.getPower() >= mData.cost * 10)
							{
								srj.sweepTimes = 10;
							}
							else
							{
								srj.sweepTimes = pi.player.getPower() / mData.cost;
							}
						}
						else
						{
							if (pi.player.getPower() >= mData.cost * sweepTimes)
							{
								srj.sweepTimes = sweepTimes;
							}
							else
							{
								srj.sweepTimes = pi.player.getPower() / mData.cost;
							}
						}
						// 模块解锁
						int mission = pi.player.getMissionId();
						int lv = pi.player.getLevel();
						String[] s = UnLockData.getUnLockStrByMethod(mission, lv);
						srj.s = s;
					}
					else
					{
						srj.errorCode = errorcode;
					}
				}
				else
				{
					srj.errorCode = -3;
				}
			}
			else
			{
				srj.errorCode = -1;
			}
			
			String msg = JSON.toJSONString(srj);
			logger.info("msg:" + msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e)
		{
			errorlogger.error(this.getClass().getName() + "->" + Thread.currentThread().getStackTrace()[1].getMethodName() + "():", e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
	
	/**
	 * KO任务判断(传输码 0,该关卡无任务或者未达成任务条件 1,该任务合体技击杀判断类型任务 2,该关卡有额外奖励并达成任务条件
	 * 3,该关卡有回合数判断类型任务)
	 **/
	public Integer missionTask(int md, PlayerInfo pi)
	{
		int missiontask = 0;
		MissionData mData = MissionData.getMissionData(md);
		if (mData.addtasktype > 0)// ==0时无任务
		{
			int addtasktype = mData.addtasktype;
			CardGroup cg = pi.getCardGroup();
			Card[] cs = cg.getCards();
			Skill[] ss = cg.getSkills();
			switch (addtasktype)
			{
				case 1:// 使用合体技击杀最后一个敌人(客户端自己判断)
					missiontask = 1;
					break;
				case 2:// 队伍中拥有指定星级卡牌
					for (int i = 0; i < cs.length; i++)
					{
						if (cs[i] != null)
						{
							CardData cData = CardData.getData(cs[i].getCardId());
							if (cData.star == mData.addtaskid)
							{
								missiontask = 2;
								break;
							}
						}
					}
					break;
				case 3:// 不适用指定星级卡牌
					boolean a = true;
					for (int i = 0; i < cs.length; i++)
					{
						if (cs[i] != null)
						{
							CardData cData = CardData.getData(cs[i].getCardId());
							if (cData.star == mData.addtaskid)
							{
								missiontask = 0;
								a = false;
								break;
							}
						}
					}
					if (a)
					{
						missiontask = 2;
					}
					break;
				case 4:// 队伍中拥有指定种族卡牌
					for (int i = 0; i < cs.length; i++)
					{
						if (cs[i] != null)
						{
							CardData cData = CardData.getData(cs[i].getCardId());
							if (cData.race == mData.addtaskid)
							{
								missiontask = 2;
								break;
							}
						}
					}
					break;
				case 5:// 不适用指定种族卡牌
					boolean b = true;
					for (int i = 0; i < cs.length; i++)
					{
						if (cs[i] != null)
						{
							CardData cData = CardData.getData(cs[i].getCardId());
							if (cData.race == mData.addtaskid)
							{
								missiontask = 0;
								b = false;
								break;
							}
						}
					}
					if (b)
					{
						missiontask = 2;
					}
					break;
				case 6:// 队伍中拥有指定卡牌
					for (int i = 0; i < cs.length; i++)
					{
						if (cs[i] != null)
						{
							if (cs[i].getCardId() == mData.addtaskid)
							{
								missiontask = 2;
								break;
							}
						}
					}
					break;
				case 7:// 不适用指定卡牌
					boolean c = true;
					for (int i = 0; i < cs.length; i++)
					{
						if (cs[i] != null)
						{
							if (cs[i].getCardId() == mData.addtaskid)
							{
								missiontask = 0;
								c = false;
								break;
							}
						}
					}
					if (c)
					{
						missiontask = 2;
					}
					break;
				case 8:// 队伍中拥有指定类型技能
					for (int i = 0; i < ss.length; i++)
					{
						if (ss[i] != null)
						{
							SkillData sData = SkillData.getData(ss[i].getSkillId());
							if (sData.type == mData.addtaskid)
							{
								missiontask = 2;
								break;
							}
						}
					}
					break;
				case 9:// 不适用指定类型技能
					boolean d = true;
					for (int i = 0; i < ss.length; i++)
					{
						if (ss[i] != null)
						{
							SkillData sData = SkillData.getData(ss[i].getSkillId());
							if (sData.type == mData.addtaskid)
							{
								missiontask = 0;
								d = false;
								break;
							}
						}
					}
					if (d)
					{
						missiontask = 2;
					}
					break;
				case 10:// 队伍中拥有指定属性技能
					for (int i = 0; i < ss.length; i++)
					{
						if (ss[i] != null)
						{
							SkillData sData = SkillData.getData(ss[i].getSkillId());
							if (sData.element == mData.addtaskid)
							{
								missiontask = 2;
								break;
							}
						}
					}
					break;
				case 11:// 不适用指定属性技能
					boolean e = true;
					for (int i = 0; i < ss.length; i++)
					{
						if (ss[i] != null)
						{
							SkillData sData = SkillData.getData(ss[i].getSkillId());
							if (sData.element == mData.addtaskid)
							{
								missiontask = 0;
								e = false;
								break;
							}
						}
					}
					if (e)
					{
						missiontask = 2;
					}
					break;
				case 12:// 队伍中拥有指定技能
					for (int i = 0; i < ss.length; i++)
					{
						if (ss[i] != null)
						{
							if (ss[i].getSkillId() == mData.addtaskid)
							{
								missiontask = 2;
								break;
							}
						}
					}
					break;
				case 13:// 不使用指定技能
					boolean f = true;
					for (int i = 0; i < ss.length; i++)
					{
						if (ss[i] != null)
						{
							if (ss[i].getSkillId() == mData.addtaskid)
							{
								missiontask = 0;
								f = false;
								break;
							}
						}
					}
					if (f)
					{
						missiontask = 2;
					}
					break;
				case 14:// 规定回合数胜利
					missiontask = 3;
					break;
				case 15:// 队伍中拥有指定星级技能
					for (int i = 0; i < ss.length; i++)
					{
						if (ss[i] != null)
						{
							SkillData sData = SkillData.getData(ss[i].getSkillId());
							if (sData.star == mData.addtaskid)
							{
								missiontask = 2;
								break;
							}
						}
					}
					break;
				default:
					break;
			}
		}
		return missiontask;
	}
	
	/** 开宝箱功能 **/
	public ModelAndView gameBoxUse(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			ModelAndView checkResult = RequestUtil.check(request);
			if (checkResult != null)
			{
				return checkResult;
			}
			// 获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			// 获取参数
			GameBoxJson gbj = JSONObject.toJavaObject(jsonObject, GameBoxJson.class);
			GameBoxResultJson gbrj = new GameBoxResultJson();
			if (gbj != null)
			{
				PlayerInfo pi = Cache.getInstance().getPlayerInfo(gbj.playerId, request.getSession());
				if (pi != null)
				{
					int useTimes = gbj.usetimes;
					int boxId = gbj.boxId;
					HashMap<String, GameBoxElement> gameBoxMap = new HashMap<String, GameBoxElement>();// goodstype-goodsid-droptype
					int errorcode = 0;
					// 校验
					// 校验宝箱和钥匙
					ItemsData iData = ItemsData.getItemsData(boxId);
					if (errorcode == 0)
					{
						if (!(iData.type == 5 || iData.type == 6 || iData.type == 2 || iData.type == 7))
						{
							errorcode = 105;
						}
					}
					if (errorcode == 0)
					{
						if (useTimes == 1)
						{
							if (useTimes < iData.use)
							{
								errorcode = 108;
							}
						}
						else if (useTimes > 1)
						{
							if (useTimes < iData.usetimes)
							{
								errorcode = 108;
							}
						}
					}
					// 校验数量
					if (errorcode == 0)
					{
						int itemNum = pi.getTargetItemsNum(boxId);
						if (itemNum < useTimes)
						{
							errorcode = 106;
						}
					}
					ItemsData itemData = null;
					if (iData.type == 5 || iData.type == 6)
					{
						GameBoxData gData0 = GameBoxData.getBoxDatas(boxId, iData.type).get(0);
						if (iData.type == 5)
						{
							itemData = ItemsData.getItemsData(gData0.keyid);
						}
						else if (iData.type == 6)
						{
							itemData = ItemsData.getItemsData(gData0.boxstar);
						}
					}
					
					// 如果是宝箱或者钥匙使用，校验对应物品的数量
					if (iData.type == 5 || iData.type == 6)
					{
						if (errorcode == 0)
						{
							int itemNum = pi.getTargetItemsNum(itemData.id);
							
							if (itemNum < useTimes)
							{
								errorcode = 107;
							}
						}
						// 宝箱使用获得物品
						if (errorcode == 0)
						{
							List<GameBoxData> list = GameBoxData.getGameBoxDatas(boxId, 0, iData.type);// 随机掉落
							List<GameBoxData> list1 = GameBoxData.getGameBoxDatas(boxId, 1, iData.type);// 固定掉落
							
							if (list != null && list.size() > 0)
							{
								for (int j = 0; j < useTimes; j++)
								{
									// 固定掉落
									for (int k = 0; k < list1.size(); k++)
									{
										boolean drop = true;
										GameBoxData gData = list1.get(k);
										if (gData.totalnumber != 0)// 判断是否有全局限制
										{
											if (Cache.gameBoxsMap == null || Cache.gameBoxsMap.size() == 0)// 如果数据库数据为空，将其加入
											{
												GameBox gBox = new GameBox();
												gBox.setId(gData.id);
												gBox.setGamebox_num(gData.totalnumber - 1);
												gBox.setGamebox_time(gData.totaltime);
												Cache.gameBoxsMap.put(gBox.getId(), gBox);
											}
											else
											{
												GameBox gBox = Cache.gameBoxsMap.get(gData.id);
												if (gBox != null)// 如果数据库中已经有此数据，判断全局数量
												{
													if (gBox.getGamebox_num() > 0)
													{
														gBox.setGamebox_num(gBox.getGamebox_num() - 1);
													}
													else
													{
														drop = false;
													}
												}
												else
												// 数据库中没有此数据，将其加入
												{
													gBox = new GameBox();
													gBox.setId(gData.id);
													gBox.setGamebox_num(gData.totalnumber - 1);
													gBox.setGamebox_time(gData.totaltime);
													Cache.gameBoxsMap.put(gBox.getId(), gBox);
												}
											}
										}
										if (drop)
										{
											if (gameBoxMap.containsKey(gData.goodstpye + "-" + gData.itemid + "-" + gData.droptpye))
											{
												GameBoxElement gbe = gameBoxMap.get(gData.goodstpye + "-" + gData.itemid + "-" + gData.droptpye);
												gbe.setNum(gbe.getNum() + gData.number);
											}
											else
											{
												GameBoxElement gbe = new GameBoxElement();
												gbe.setGoodsType(gData.goodstpye);
												gbe.setGoodsId(gData.itemid);
												gbe.setNum(gData.number);
												gbe.setDropType(gData.droptpye);
												gameBoxMap.put(gData.goodstpye + "-" + gData.itemid + "-" + gData.droptpye, gbe);
											}
										}
									}
									// 随机掉落
									int st = 0;
									int end = 0;
									int randNum = Random.getNumber(0, Constant.TotalDropPro);
									boolean a = false;
									for (int i = 0; i < list.size(); i++)
									{
										boolean drop = true;
										GameBoxData gData = list.get(i);
										if (gData.totalnumber != 0)// 判断是否有全局限制
										{
											if (Cache.gameBoxsMap == null || Cache.gameBoxsMap.size() == 0)// 如果数据库数据为空，将其加入
											{
												GameBox gBox = new GameBox();
												gBox.setId(gData.id);
												gBox.setGamebox_num(gData.totalnumber);
												gBox.setGamebox_time(gData.totaltime);
												Cache.gameBoxsMap.put(gBox.getId(), gBox);
											}
											else
											{
												GameBox gBox = Cache.gameBoxsMap.get(gData.id);
												if (gBox != null)// 数据库中没有此数据，将其加入
												{
													if (gBox.getGamebox_num() <= 0)
													{
														drop = false;
													}
												}
												else
												{
													gBox = new GameBox();
													gBox.setId(gData.id);
													gBox.setGamebox_num(gData.totalnumber);
													gBox.setGamebox_time(gData.totaltime);
													Cache.gameBoxsMap.put(gBox.getId(), gBox);
												}
											}
										}
										if (gData.probability > 0)
										{
											if (pi.player.getVipCost() >= gData.cost)
											{
												end = gData.probability + st;
												if (randNum >= st && randNum <= end)
												{
													if (drop)
													{
														if (gameBoxMap.containsKey(gData.goodstpye + "-" + gData.itemid + "-" + gData.droptpye))
														{
															GameBoxElement gbe = gameBoxMap.get(gData.goodstpye + "-" + gData.itemid + "-" + gData.droptpye);
															gbe.setNum(gbe.getNum() + gData.number);
														}
														else
														{
															GameBoxElement gbe = new GameBoxElement();
															gbe.setGoodsType(gData.goodstpye);
															gbe.setGoodsId(gData.itemid);
															gbe.setNum(gData.number);
															gbe.setDropType(gData.droptpye);
															gameBoxMap.put(gData.goodstpye + "-" + gData.itemid + "-" + gData.droptpye, gbe);
														}
														a = true;
														GameBox gBox = Cache.gameBoxsMap.get(gData.id);
														if (gBox != null)
														{
															gBox.setGamebox_num(gBox.getGamebox_num() - 1);
														}
													}
													break;
												}
												else
												{
													st = end;
												}
											}
										}
									}
									if (!a)// 如果没有随机出来物品，则第一个物品必随出
									{
										if (gameBoxMap.containsKey(list.get(0).goodstpye + "-" + list.get(0).itemid + "-" + list.get(0).droptpye))
										{
											GameBoxElement gbe = gameBoxMap.get(list.get(0).goodstpye + "-" + list.get(0).itemid + "-" + list.get(0).droptpye);
											gbe.setNum(gbe.getNum() + list.get(0).number);
										}
										else
										{
											GameBoxElement gbe = new GameBoxElement();
											gbe.setGoodsType(list.get(0).goodstpye);
											gbe.setGoodsId(list.get(0).itemid);
											gbe.setNum(list.get(0).number);
											gbe.setDropType(list.get(0).droptpye);
											gameBoxMap.put(list.get(0).goodstpye + "-" + list.get(0).itemid + "-" + list.get(0).droptpye, gbe);
										}
									}
								}
							}
						}
					}
					if (iData.type == 7)// 宝物袋
					{
						SackData sackData = SackData.getSackData(boxId);
						if (errorcode == 0)
						{
							if (pi.player.getLevel() < sackData.uselevel)
							{
								errorcode = 57;
							}
						}
						if (errorcode == 0)
						{
							if (pi.player.getVipLevel() < sackData.viprequest)
							{
								errorcode = 70;
							}
						}
						if (errorcode == 0)
						{
							if (pi.getTargetItemsNum(boxId) < sackData.cost * useTimes)
							{
								errorcode = 20;
							}
						}
						// 宝物袋使用之后随机物品
						if (errorcode == 0)
						{
							List<String> list = sackData.rewards;
							
							for (int j = 0; j < useTimes; j++)
							{
								int number = 1;
								if (sackData.timesmax == sackData.timesmin)
								{
									number = sackData.timesmax;
								}
								else
								{
									number = Random.getNumber(sackData.timesmin, sackData.timesmax);
								}
								int sackFirst = 0;
								sackFirst = pi.player.getSackFirst();
								for (int k = 0; k < number; k++)
								{
									int m = 0;
									if (list.size() > 0)
									{
										String[] temp0 = list.get(0).split("-");
										int itemType = StringUtil.getInt(temp0[0]);
										String[] itemId = temp0[1].split(",");
										switch (itemType)
										{
											
											case 1:// item
												if (pi.getTargetItemsNum(StringUtil.getInt(itemId[0])) >= sackData.switchpoint)
												{
													m = 1;
												}
												break;
											default:// equip,card,skill,ps
												if (pi.getTargetNum(itemType, StringUtil.getInt(itemId[0])) >= sackData.switchpoint)
												{
													m = 1;
												}
												break;
										}
									}
									int randNum = Random.getNumber(0, Constant.TotalDropPro);
									int st = 0;
									int end = 0;
									
									for (int i = 0; i < list.size(); i++)
									{
										String str = "";
										if (sackFirst == 0)
										{
											str = list.get(0);
										}
										else
										{
											str = list.get(i);
										}
										if (str != null && str.length() > 0)
										{
											String[] temp = str.split("-");
											if (sackFirst == 0)
											{
												if (temp[1] != null && temp[1].length() > 0)
												{
													String key = "";
													int num = 0;
													int goosId = 0;
													if (StringUtil.getInt(temp[0]) <= 5)
													{
														String[] ss = temp[1].split(",");
														key = temp[0] + "-" + ss[0] + "-" + 0;
														num = StringUtil.getInt(ss[1]);
														goosId = StringUtil.getInt(ss[0]);
													}
													else
													{
														key = temp[0] + "-" + temp[1] + "-" + 0;
														num = StringUtil.getInt(temp[1]);
														goosId = StringUtil.getInt(temp[1]);
													}
													// 总和物品总数
													if (gameBoxMap.containsKey(key))
													{
														GameBoxElement gbe = gameBoxMap.get(key);
														gbe.setNum(gbe.getNum() + num);
													}
													else
													{
														GameBoxElement gbe = new GameBoxElement();
														gbe.setDropType(0);
														gbe.setGoodsId(goosId);
														gbe.setNum(num);
														gbe.setGoodsType(StringUtil.getInt(temp[0]));
														gameBoxMap.put(key, gbe);
													}
												}
												sackFirst = 1;
												break;
											}
											else
											{
												end = StringUtil.getInt(temp[2 + m]) + st;
												if (randNum >= st && randNum < end)
												{
													if (temp[1] != null && temp[1].length() > 0)
													{
														String key = "";
														int num = 0;
														int goosId = 0;
														if (StringUtil.getInt(temp[0]) <= 5)
														{
															String[] ss = temp[1].split(",");
															key = temp[0] + "-" + ss[0] + "-" + 0;
															num = StringUtil.getInt(ss[1]);
															goosId = StringUtil.getInt(ss[0]);
														}
														else
														{
															key = temp[0] + "-" + temp[1] + "-" + 0;
															num = StringUtil.getInt(temp[1]);
															goosId = StringUtil.getInt(temp[1]);
														}
														// 总和物品总数
														if (gameBoxMap.containsKey(key))
														{
															GameBoxElement gbe = gameBoxMap.get(key);
															gbe.setNum(gbe.getNum() + num);
														}
														else
														{
															GameBoxElement gbe = new GameBoxElement();
															gbe.setDropType(0);
															gbe.setGoodsId(goosId);
															gbe.setNum(num);
															gbe.setGoodsType(StringUtil.getInt(temp[0]));
															gameBoxMap.put(key, gbe);
														}
													}
													break;
												}
												else
												{
													st = end;
												}
											}
										}
									}
								}
								
							}
						}
					}
					
					// 如果消耗品，校验等级和vip等级
					if (iData.type == 2)
					{
						UseableItemData uData = UseableItemData.getUseableItemData(boxId);
						if (errorcode == 0)
						{
							if (pi.player.getLevel() < uData.uselevel)
							{
								errorcode = 57;
							}
						}
						if (errorcode == 0)
						{
							if (pi.player.getVipLevel() < uData.viprequest)
							{
								errorcode = 70;
							}
						}
						if (errorcode == 0)// 校验材料数量
						{
							if (pi.getTargetItemsNum(boxId) < uData.cost * useTimes)
							{
								errorcode = 20;
							}
						}
						// 消耗品使用之后获得物品
						if (errorcode == 0)
						{
							List<String> list = uData.rewardInfo;
							for (int j = 0; j < useTimes; j++)
							{
								for (int i = 0; i < list.size(); i++)
								{
									String str = list.get(i);
									if (str != null && str.length() > 0)
									{
										String[] temp = str.split("-");
										if (temp[1] != null && temp[1].length() > 0)
										{
											String key = "";
											int num = 0;
											int goosId = 0;
											if (StringUtil.getInt(temp[0]) <= 5)
											{
												String[] ss = temp[1].split(",");
												key = temp[0] + "-" + ss[0] + "-" + 0;
												num = StringUtil.getInt(ss[1]);
												goosId = StringUtil.getInt(ss[0]);
											}
											else
											{
												key = temp[0] + "-" + temp[1] + "-" + 0;
												num = StringUtil.getInt(temp[1]);
												goosId = StringUtil.getInt(temp[1]);
											}
											// 总和物品总数
											if (gameBoxMap.containsKey(key))
											{
												GameBoxElement gbe = gameBoxMap.get(key);
												gbe.setNum(gbe.getNum() + num);
											}
											else
											{
												GameBoxElement gbe = new GameBoxElement();
												gbe.setDropType(0);
												gbe.setGoodsId(goosId);
												gbe.setNum(num);
												gbe.setGoodsType(StringUtil.getInt(temp[0]));
												gameBoxMap.put(key, gbe);
											}
										}
									}
								}
							}
						}
					}
					// 校验背包
					if (errorcode == 0)
					{
						boolean card = true;
						boolean skill = true;
						boolean pskill = true;
						boolean equip = true;
						boolean item = true;
						for (GameBoxElement gbe : gameBoxMap.values())
						{
							if (gbe.getGoodsType() == 1)// item
							{
								if (item)
								{
									if (pi.getItems().size() < Statics.getPackageNum(pi))
									{
										item = false;
									}
									else
									{
										errorcode = 53;
										break;
									}
								}
							}
							if (gbe.getGoodsType() == 2)// equip
							{
								if (equip)
								{
									if (pi.getEquips().size() < Statics.getPackageNum(pi))
									{
										equip = false;
									}
									else
									{
										errorcode = 53;
										break;
									}
								}
							}
							if (gbe.getGoodsType() == 3)// card
							{
								if (card)
								{
									if (pi.getCards().size() < Statics.getPackageNum(pi))
									{
										card = false;
									}
									else
									{
										errorcode = 53;
										break;
									}
								}
							}
							if (gbe.getGoodsType() == 4)// skill
							{
								if (skill)
								{
									if (pi.getSkills().size() < Statics.getPackageNum(pi))
									{
										skill = false;
									}
									else
									{
										errorcode = 53;
										break;
									}
								}
							}
							if (gbe.getGoodsType() == 5)// pskill
							{
								if (pskill)
								{
									if (pi.getPassiveSkillls().size() < Statics.getPackageNum(pi))
									{
										pskill = false;
									}
									else
									{
										errorcode = 53;
										break;
									}
								}
							}
						}
					}
					// 功能阶段
					if (errorcode == 0)
					{
						List<GameBoxElement> gbes = new ArrayList<GameBoxElement>();
						if (iData.type == 6 || iData.type == 5)// 宝箱或者钥匙
						{
							// 消耗
							pi.removeItem(boxId, useTimes);
							pi.removeItem(itemData.id, useTimes);
							playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|请求开宝箱");
						}
						else if (iData.type == 2)// 消耗品
						{
							UseableItemData uData = UseableItemData.getUseableItemData(boxId);
							// 消耗
							pi.removeItem(boxId, useTimes * uData.cost);
							playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|请求消耗品使用");
						}
						else if (iData.type == 7)
						{
							SackData sData = SackData.getSackData(boxId);
							pi.removeItem(boxId, useTimes * sData.cost);
							if (pi.player.getSackFirst() == 0)
							{
								pi.player.setSackFirst(1);
							}
							playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|请求宝物袋");
						}
						List<Integer> cardIds = new ArrayList<Integer>();
						if (gameBoxMap != null && gameBoxMap.size() > 0)
						{
							for (GameBoxElement gbe : gameBoxMap.values())
							{
								gbes.add(gbe);
								if (gbe.getGoodsType() == 3)
								{
									cardIds.add(gbe.getGoodsId());
								}
								Statics.getReward(gbe.getGoodsType(), gbe.getGoodsId(), gbe.getNum(), pi);
							}
							if (cardIds.size() > 0)// 更新解锁合体技
							{
								pi.getNewUnitSkill(cardIds);
							}
						}
						gbrj.ges = gbes;
					}
					else
					{
						gbrj.errorCode = errorcode;
						playlogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + pi.player.getId() + "|" + pi.player.getName() + "|等级：" + pi.player.getLevel() + "|请求开宝箱或者消耗品使用");
					}
				}
				else
				{
					gbrj.errorCode = -3;
				}
			}
			else
			{
				gbrj.errorCode = -1;
			}
			String msg = JSON.toJSONString(gbrj);
			logger.info("msg:" + msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e)
		{
			errorlogger.error(this.getClass().getName() + "->" + Thread.currentThread().getStackTrace()[1].getMethodName() + "():", e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
	
	private void lotRank(long curTime, PlayerInfo pi, TakeCardTimeData tctd)
	{
		LotRank l = null;
		LotRank lr = Cache.getInstance().getLotRankById(pi.player.getId());
		int cardId = 0;
		// 更具抽卡次数确定卡组
		CardkuData ck1 = CardkuData.getCardkuData2(1);
		CardkuData ck2 = CardkuData.getCardkuData2(2);
		CardkuData ck3 = CardkuData.getCardkuData2(3);
		int num = lr.getLotNum();
		if (lr != null)
		{
			if (num > ck3.firstjump)
			{
				if (num % ck2.secondjump == 0)
				{
					cardId = CardkuData.getCardIdByKuType(2);
				}
				else if (num % ck3.secondjump == 0 || (num % ck3.secondjump == 0 && num % ck2.secondjump == 0))
				{
					cardId = CardkuData.getCardIdByKuType(3);
				}
				else
				{
					cardId = CardkuData.getCardIdByKuType(1);
				}
			}
			else
			{
				if (num == ck2.firstjump)
				{
					cardId = CardkuData.getCardIdByKuType(2);
				}
				else if (num == ck3.firstjump)
				{
					cardId = CardkuData.getCardIdByKuType(3);
				}
				else
				{
					cardId = CardkuData.getCardIdByKuType(1);
				}
			}
			if (cardId != 0)
			{
				pi.addCard(cardId, 0);
				lr.setLastLotTime(curTime);
				lr.setLotNum(lr.getLotNum() + 1);
				lr.setScore(lr.getScore() + tctd.getintegral);
			}
			else
			{
				logger.info("没有抽到卡");
			}
		}
		else
		{
			l = new LotRank();
			l.setPlayerId(pi.player.getId());
			l.setScore(tctd.getintegral);
			l.setLastLotTime(curTime);
			l.setLotNum(1);
			cardId = CardkuData.getCardIdByKuType(1);
			if (cardId != 0)
			{
				pi.addCard(cardId, 0);
				Cache.getInstance().addLotRanks(l);
			}
		}
		// Card c = pi.addCard(cardId, 1);
		// if (c != null)
		// {
		// int index = pi.getCards().indexOf(c);
		// PackElement pe = Statics.createPackElement(index, c, pi);
		// pe.nw = ownCardIds.contains(pe.dataId) ? 0 : 1;
		// lrj.list.add(pe);
		// }
	}
}
