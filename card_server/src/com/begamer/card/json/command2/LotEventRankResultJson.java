package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;

public class LotEventRankResultJson extends ErrorJson {
	
	public int cs;// 抽卡池id
	public List<String> sr;// 积分排行榜
	public int score;// 积分
	public int rank;// 排名
	public int crystal;// 钻石
	public String bt;// 活动开始时间
	public String et;// 活动结束时间
	public int ht;// 活动倒计时
	public int lt;// 抽卡冷却时间
	public int uc;// 抽卡花费钻石
	public int n;//下次换卡次数
	
	public int getCs()
	{
		return cs;
	}
	
	public void setCs(int cs)
	{
		this.cs = cs;
	}
	
	public List<String> getSr()
	{
		return sr;
	}
	
	public void setSr(List<String> sr)
	{
		this.sr = sr;
	}
	
	public int getScore()
	{
		return score;
	}
	
	public void setScore(int score)
	{
		this.score = score;
	}
	
	public int getRank()
	{
		return rank;
	}
	
	public void setRank(int rank)
	{
		this.rank = rank;
	}
	
	public int getCrystal()
	{
		return crystal;
	}
	
	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}
	
	public String getBt()
	{
		return bt;
	}
	
	public void setBt(String bt)
	{
		this.bt = bt;
	}
	
	public String getEt()
	{
		return et;
	}
	
	public void setEt(String et)
	{
		this.et = et;
	}
	
	public int getHt()
	{
		return ht;
	}
	
	public void setHt(int ht)
	{
		this.ht = ht;
	}
	
	public int getLt()
	{
		return lt;
	}
	
	public void setLt(int lt)
	{
		this.lt = lt;
	}
	
	public int getUc()
	{
		return uc;
	}
	
	public void setUc(int uc)
	{
		this.uc = uc;
	}

	public int getN()
	{
		return n;
	}

	public void setN(int n)
	{
		this.n = n;
	}
	
}
