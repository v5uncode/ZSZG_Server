package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class ActivityInfoJson extends BasicJson{

	public int id;//所属活动id
	public int aid;//活动id
	public int type;//类型
	
	public int getAid()
	{
		return aid;
	}

	public void setAid(int aid)
	{
		this.aid = aid;
	}

	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
