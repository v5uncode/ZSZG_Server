package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class ImaginationClickResultJson extends ErrorJson 
{
	/**激活NPC的id**/
	public int id;
	/**获得物品:物品类型-物品id   1,垃圾 2,被动技能 3,碎片**/
	public String s;
	/**玩家当前金币**/
	public int g;
	/**当前玩家激活的领奖id**/
	public int mid;
	/**当前玩家冥想次数**/
	public int mnum;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getS() {
		return s;
	}
	public void setS(String s) {
		this.s = s;
	}
	public int getG() {
		return g;
	}
	public void setG(int g) {
		this.g = g;
	}
	public int getMid()
	{
		return mid;
	}
	public void setMid(int mid)
	{
		this.mid = mid;
	}
	public int getMnum()
	{
		return mnum;
	}
	public void setMnum(int mnum)
	{
		this.mnum = mnum;
	}
	
}
