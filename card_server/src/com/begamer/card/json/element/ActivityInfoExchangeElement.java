package com.begamer.card.json.element;

import java.util.List;


public class ActivityInfoExchangeElement {

	public int id;
	public int exchangeType;// 兑换物品类型
	public int exchangeId;// 兑换物品id
	public int exchangeNum;// 兑换物品数量
	public String exchangeContext;//兑换描述
	public int sole;// 兑换次数 0,无限制
	public int pSole;//玩家兑换次数
	public int sell;// 是否可以兑换(0不能兑换,1兑换,3物品不足)
	public List<ActivityDElement> ade;
	
	
	public int getSell()
	{
		return sell;
	}
	public void setSell(int sell)
	{
		this.sell = sell;
	}
	public String getExchangeContext()
	{
		return exchangeContext;
	}
	public void setExchangeContext(String exchangeContext)
	{
		this.exchangeContext = exchangeContext;
	}
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getExchangeType()
	{
		return exchangeType;
	}
	public void setExchangeType(int exchangeType)
	{
		this.exchangeType = exchangeType;
	}
	public int getExchangeId()
	{
		return exchangeId;
	}
	public void setExchangeId(int exchangeId)
	{
		this.exchangeId = exchangeId;
	}
	public int getExchangeNum()
	{
		return exchangeNum;
	}
	public void setExchangeNum(int exchangeNum)
	{
		this.exchangeNum = exchangeNum;
	}
	public int getSole()
	{
		return sole;
	}
	public void setSole(int sole)
	{
		this.sole = sole;
	}
	public List<ActivityDElement> getAde()
	{
		return ade;
	}
	public void setAde(List<ActivityDElement> ade)
	{
		this.ade = ade;
	}
	public int getPSole()
	{
		return pSole;
	}
	public void setPSole(int sole)
	{
		pSole = sole;
	}
}
